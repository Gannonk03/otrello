//
//  MoveCommand.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

enum Direction: String {
    case Up
    case Down
    case Left
    case Right
}

enum BlockColor: String {
    case Red
    case White
}

enum MoveOrder {
    case MoveOrder(source: Int, destination: Int, withTrail: Bool)
}



