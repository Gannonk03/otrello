//
//  BoardCoordinate.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

struct BoardCoordinate : Hashable {
    var row: Int
    var col: Int
    
    init(row: Int, col: Int) {
        self.row = row
        self.col = col
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine((31 &* row.hashValue) &+ col.hashValue)
    }
}

func == (lhs: BoardCoordinate, rhs: BoardCoordinate) -> Bool {
    return
        (lhs.row.hashValue == rhs.row.hashValue) &&
        (lhs.col.hashValue == rhs.col.hashValue)
}
