//
//  Tile.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

enum TileType: String {
    case Mover
    case Blocker
    case Empty
    case NotInPlay
}

protocol Tile {
    var type: TileType { get }
}

struct Mover: Tile {
    var color: BlockColor
    var type: TileType = .Mover
    
    init(blockColor: BlockColor) {
        self.color = blockColor
    }
}

struct Blocker: Tile {
    var type: TileType = .Blocker
}

struct Empty: Tile {
    var type: TileType = .Empty
}

struct NotInPlay: Tile {
    var type: TileType = .NotInPlay
}

