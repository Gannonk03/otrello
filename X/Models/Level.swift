//
//  Level.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//
import UIKit

struct Level {
    var id: Int
    var rows: Int
    var cols: Int
    var board: [Tile]
    var corners: [CornerProps]
    var bgTextureType: TextureType
    var boardData: GameBoardData
}
