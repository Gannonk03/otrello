//
//  Game.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class GameModel {
    private var queue = [Direction]()
    private var gameBoard: [Tile]
    private var queueTimer = Timer()
    
    private let rows: Int
    private let cols: Int
    private let queueDelay = 0.2
    
    static let maxCols: Int = 8
    static let maxRows: Int = 10
    
    weak var delegate: GameProtocol?
    
    init(
        rows: Int,
        cols: Int,
        tiles: [Tile]
    ) {
        self.rows = rows
        self.cols = cols
        self.gameBoard = tiles
    }
    
    func slide(direction: Direction) {
        queue.append(direction)
        
        if !queueTimer.isValid {
            executeMoveCommand()
        }
    }
    
    @objc func executeMoveCommand() {
        if queue.count == 0 { return }
        
        while queue.count > 0 {
            let command = queue[0]
            queue.remove(at: 0)
            performSlide(direction: command)
        }
        
        setQueueTimer()
    }
    
    func resetLevel(tiles: [Tile]) {
        self.gameBoard = tiles
        
        queueTimer.invalidate()
        queue = [Direction]()
    }
    
    private func setQueueTimer() {
        queueTimer = Timer.scheduledTimer(
            timeInterval: queueDelay,
            target: self,
            selector: #selector(GameModel.executeMoveCommand),
            userInfo: nil,
            repeats: false
        )
    }
    
    private func getFromGameboard(row: Int, col: Int) -> Tile {
        return gameBoard[row * cols + col]
    }
    
    private func setOnGameboard(row: Int, col: Int, tile: Tile) {
        gameBoard[row * cols + col] = tile
    }
    
    private func coordinateGenerator(
        direction: Direction,
        iteration: Int
    ) -> [(Int,Int)] {
        
        var count: Int
        switch direction {
        case .Down, .Up:
            count = self.rows
        default:
            count = self.cols
        }
        var buffer = Array<(Int,Int)>(repeating: (0,0), count: count)
        for i in 0..<count {
            switch direction {
            case .Down: buffer[i] = (i,iteration)
            case .Up: buffer[i] = (rows - i - 1, iteration)
            case .Left: buffer[i] = (iteration,i)
            case .Right: buffer[i] = (iteration,cols - i - 1)
            }
        }
        
        return buffer
    }
    
    private func performSlide(direction: Direction) {
        var reds = [BoardCoordinate]()
        var to: Int
        switch direction {
        case .Down,.Up:
            to = self.cols
        default:
            to = self.rows
        }
        for i in 0..<to {
            let coords = coordinateGenerator(direction: direction, iteration: i)
            let tiles = coords.map() { (coordinate: (Int,Int)) -> Tile in
                let (x,y) = coordinate
                return self.getFromGameboard(row: x, col: y)
            }
            
            var playSound = false
            let orders = getOrders(group: tiles, direction: direction)
            for order in orders {
                switch order {
                case let .MoveOrder(source: s, destination: d, withTrail: t):
                    updateTilePositions(source: s, destination: d, coords: coords)
                    delegate?.moveTile(from: coords[s], to: coords[d], withTrail: t)
                    if s != d { playSound = true }
                    let (destinationRow,destinationCol) = coords[d]
                    let tile = getFromGameboard(
                        row: destinationRow,
                        col: destinationCol
                    )
                    if let red = tile as? Mover, red.color == .Red {
                        reds.append(BoardCoordinate(
                            row: destinationRow,
                            col: destinationCol
                        ))
                    }
                }
            }
            
            if playSound {
                delegate?.playSlideSound()
            }
        }
        var playPopSound = false
        var redsCpy = reds
        while !reds.isEmpty {
            let last = reds.remove(at: reds.count-1)
            for red in redsCpy where red != last {
                let coor1 = last
                let coor2 = red
                
                if coor1.col == coor2.col {
                    let start = min(coor1.row,coor2.row) + 1
                    let end = max(coor1.row,coor2.row)
                    var coords = [BoardCoordinate]()
                    for i in start..<end {
                        if let white =
                            getFromGameboard(row: i, col: coor1.col) as? Mover,
                            white.color == .White {
                            coords.append(BoardCoordinate(
                                row: i,
                                col: coor1.col
                            ))
                        }
                        else {
                            coords.removeAll()
                            break
                        }
                    }
                    for coor in coords {
                        reds.append(coor)
                        redsCpy.append(coor)
                        playPopSound = true
                        delegate?.flipTile(at: (coor.row,coor.col))
                        setOnGameboard(
                            row: coor.row,
                            col: coor.col,
                            tile: Mover(blockColor: .Red
                        ))
                    }
                }
                
                if coor1.row == coor2.row {
                    let start = min(coor1.col,coor2.col) + 1
                        let end = max(coor1.col,coor2.col)
                        var coords = [BoardCoordinate]()
                        for i in start..<end {
                            if let white =
                                getFromGameboard(row: coor1.row, col: i) as? Mover,
                                white.color == .White {
                                coords.append(BoardCoordinate(
                                    row: coor1.row,
                                    col: i
                                ))
                            }
                            else {
                                coords.removeAll()
                                break
                            }
                        }
                        for coor in coords {
                            reds.append(coor)
                            redsCpy.append(coor)
                            playPopSound = true
                            delegate?.flipTile(at: (coor.row,coor.col))
                            setOnGameboard(
                                row: coor.row,
                                col: coor.col,
                                tile: Mover(blockColor: .Red
                            ))
                        }
                    }
                }
            }
        if playPopSound {
            delegate?.playPopSound()
        }
    }
    
    private func updateTilePositions(
        source: Int,
        destination: Int?,
        coords: [(Int,Int)]
    ) {
        let (sourceRow,sourceCol) = coords[source]
        
        let tile = getFromGameboard(row: sourceRow, col: sourceCol)
        
        setOnGameboard(row: sourceRow, col: sourceCol, tile: Empty())
        
        if let dest = destination {
            let (destinationRow,destinationCol) = coords[dest]
            setOnGameboard(row: destinationRow, col: destinationCol, tile: tile)
        }
    }
    
    private func getOrders(
        group: [Tile],
        direction: Direction
    ) -> [MoveOrder]  {
        var moveBuffer = [MoveOrder]()
        var openIndex = 0
        
        for (groupIndex,tile) in group.enumerated() {
            switch tile.type {
            case .Mover:
                var withTrail: Bool = false
                if groupIndex != openIndex {
                    let nextIndex = groupIndex + 1
                    if nextIndex >= group.count {
                        withTrail = true
                    }
                    else if group[nextIndex].type != .Mover {
                        withTrail = true
                    }
                }
                moveBuffer.append(.MoveOrder(
                    source: groupIndex,
                    destination: openIndex,
                    withTrail: withTrail
                    )
                )
                                
                openIndex = openIndex + 1
            
            case .Blocker, .NotInPlay:
                openIndex = groupIndex + 1
        
            case .Empty:
                break
            }
        }
                
        return moveBuffer
    }
}
