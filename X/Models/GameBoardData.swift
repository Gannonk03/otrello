//
//  GameBoardData.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

struct GameBoardData {
    var boardWidth: CGFloat
    var boardHeight: CGFloat
    var tileWidth: CGFloat
    var padding: CGFloat
    var textures: Dictionary<TextureType, SKTexture?>
    
    func textureFromType(type: TextureType) -> SKTexture? {
        return textures[type, default: nil]
    }
}

struct CornerProps {
    var topLeft: Bool
    var topRight: Bool
    var bottomLeft: Bool
    var bottomRight: Bool
}

enum TextureType: String {
    case Empty
    case Red
    case NotInPlay
    case White
    case Blocker
    case TailRed
    case TailWhite
    case TopLeftCorner
    case TopRightCorner
    case BottomLeftCorner
    case BottomRightCorner
    case TopLeftTopRightCorner
    case TopLeftBottomLeftCorner
    case TopRightBottomRightCorner
    case BottomLeftBottomRightCorner
    case bgTypeA
    case bgTypeB
    case bgTypeC
    case Star
    
    func textureTypeWithCornerProps(_ props: CornerProps) -> TextureType {
        switch self {
        case .Empty:
            if props.topLeft && props.topRight {
                return .TopLeftTopRightCorner
            }
            if props.topLeft && props.bottomLeft {
                return .TopLeftBottomLeftCorner
            }
            if props.topRight && props.bottomRight {
                return .TopRightBottomRightCorner
            }
            if props.bottomRight && props.bottomLeft {
                return .BottomLeftBottomRightCorner
            }
            if props.topRight {
                return .TopRightCorner
            }
            if props.topLeft {
                return .TopLeftCorner
            }
            if props.bottomRight {
                return .BottomRightCorner
            }
            if props.bottomLeft {
                return .BottomLeftCorner
            }
            return .Empty
        default:
            return self
        }
    }
}

enum GameState {
    case Active
    case TutorialActive(solution: [Direction], index: Int)
    case TutorialInactive(solution: [Direction], index: Int)
    case Over
    case Completed
    case Paused
    case TransitionPaused
    case TransitionActive
    case Home
}
