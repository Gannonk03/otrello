//
//  EmptyTileView.swift
//  X
//
//  Created by Kevin Gannon on 2/29/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import UIKit

class EmptyTileView: UIView {
    
    init(
        color: UIColor,
        length: CGFloat
    ) {
        super.init(frame: CGRect(
            x: 0,
            y: 0,
            width: length + 4,
            height: length + 4
        ))
        
        self.layer.borderWidth = 2.0
        self.layer.borderColor = UIColor.init(white: 1, alpha: 0.5).cgColor
        self.backgroundColor = UIColor.init(white: 1, alpha: 0.1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
