//
//  GameViewController.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit


enum LevelState {
    case Current
    case Previous
    case Next
}

protocol GameProtocol: class {    
    func moveTile(from: (Int, Int), to: (Int, Int), withTrail: Bool)
    func flipTile(at: (Int, Int))
    func playSlideSound()
    func playPopSound()
}

protocol SceneProtocol: class {
    func levelWon()
    func sceneLoaded()
    func isLevelCompleted(id: Int) -> Bool
    func level(state: LevelState) -> Level?
    
    var muted: Bool { get }
}

protocol MenuDelegate: class {
    func volumeOff()
    func volumeOn()
    func redoSelected() -> (() -> Void)!
    func rateSelected()
}

class GameController: UIViewController,
                      GameProtocol,
                      SceneProtocol,
                      GameViewDelegate {
    
    private var levelDataSource: LevelDataSource!
    private var model: GameModel!
    private var scene: GameScene!
    private var gameView: GameView!
    private var currentLevelID = 1
    private var completedLevels = Set<Int>()
    private var gameState: GameState = .Paused
    
    var muted: Bool = false
            
    override func loadView() {
        view = GameView(frame: UIScreen.main.bounds)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        gameView = view as? GameView
        gameView.delegate = self
        
        levelDataSource = LevelDataSource(viewWidth: gameView.bounds.width)
        
        setCurrentLevelToLastPlayed()
        setCompletedLevels()
        
        let currentLevel = levelDataSource.getLevel(levelID: currentLevelID)
        scene = GameScene(size: gameView.bounds.size)
        scene.sceneDelegate = self
        gameView.presentScene(scene: scene)
        
        model = GameModel(
            rows: currentLevel!.rows,
            cols: currentLevel!.cols,
            tiles: currentLevel!.board
        )
        
        model.delegate = self
    }
    
    //MARK: - SceneProtocol
    func levelWon() {
        switch gameState {
        case .Active:
            break
        default:
            return
        }
        
        gameState = .Completed

        completedLevels.insert(currentLevelID)
        updateCompletedLevels()
        
        if currentLevelID == 1 {
            markTutorialCompleted()
        }
        
        gameView.showLevelCompleted()
    }
    
    func isLevelCompleted(id: Int) -> Bool {
        completedLevels.contains(id)
    }
    
    func sceneLoaded() {
        gameView.setRedoEnabled(enabled: true)

        switch gameState {
        case .Paused, .TransitionActive, .Over, .TransitionPaused, .Completed:
            gameState = .Active
        default:
            return
        }
    }
    
    func level(state: LevelState) -> Level? {
        switch state {
        case .Current:
            return levelDataSource.getLevel(levelID: currentLevelID)
        case .Previous:
            if currentLevelID == 1 { return levelDataSource.getLevel(levelID: levelDataSource.maxLevel) }
            return levelDataSource.getLevel(levelID: currentLevelID - 1)
        case .Next:
            return levelDataSource.getLevel(levelID: currentLevelID + 1)
        }
    }
    
    //MARK: MenuDelegate
    func volumeOff() {
        muted = true
        scene.stopMusic()
    }
    
    func volumeOn() {
        muted = false
        scene.startMusic()
    }
    
    func nextLevelSelected() {
        guard let level = level(state: .Next) else { return }
        
        switch gameState {
        case .Paused:
            gameState = .TransitionPaused
        default:
            break
        }
         
        currentLevelID = level.id
        updateLastLevelPlayed()
        model = GameModel(
            rows: level.rows,
            cols: level.cols,
            tiles: level.board
        )
        model.delegate = self
        gameView.setRedoEnabled(enabled: false)
        scene.loadNextLevel {
            self.updateNextLevelNavigationOption()
            self.updatePreviousLevelNavigationOption()
        }
        
        updatePreviousLevelNavigationOption(onlyOnDisabled: true)
        updateNextLevelNavigationOption(onlyOnDisabled: true)
    }
    
    func previousLevelSelected() {
        guard let level = level(state: .Previous) else { return }

        gameState = .TransitionPaused
        currentLevelID = level.id
        updateLastLevelPlayed()
        model = GameModel(
            rows: level.rows,
            cols: level.cols,
            tiles: level.board
        )
        model.delegate = self
        gameView.setRedoEnabled(enabled: false)
        scene.loadPreviousLevel {
            self.updatePreviousLevelNavigationOption()
            self.updateNextLevelNavigationOption()
        }
        
        updatePreviousLevelNavigationOption(onlyOnDisabled: true)
        updateNextLevelNavigationOption(onlyOnDisabled: true)
    }
    
    func rateSelected() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "1516130019") {
           UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func redoSelected() -> (() -> Void)! {
        gameState = .Over
        resetLevel()
        gameView.isUserInteractionEnabled = false
        
        return {[weak self] in
            self?.gameView.closeMenu()
            self?.gameView.isUserInteractionEnabled = true
        }
    }
    
    //MARK: GameProtocol
    func moveTile(from: (Int, Int), to: (Int, Int), withTrail: Bool) {
        scene.moveTile(from: from, to: to, withTrail: withTrail)
    }
    
    func flipTile(at: (Int, Int)) {
        scene.flipTile(at: at)
    }
    
    func playSlideSound() {
        if muted { return }
        scene.playSlideSound()
    }
    
    func playPopSound() {
        if muted { return }
        scene.playPopSound()
    }
    
    //MARK: - GameViewDelegate
    func slide(direction: Direction) {
        switch gameState {
        case .Active:
            model.slide(direction: direction)
        case let .TutorialActive(solution: s, index: i):
            if direction != s[i] { return }
        default:
            return
        }
    }
    
    func boardWidth() -> CGFloat {
        return level(state: .Current)!.boardData.boardWidth
    }
    
    func menuWillOpen() -> Bool {
        switch gameState {
        case .Active:
            gameState = .Paused
            return true
        default:
            return false
        }
    }
    
    func menuWillClose() {
        switch gameState {
        case .TransitionPaused:
            gameState = .TransitionActive
        default:
            gameState = .Active
        }
    }
    
    //MARK: Private
    
    private func resetLevel() {
        guard let current = level(state: .Current) else { return }
        model.resetLevel(tiles: current.board)
        scene.restartLevel()
    }
    
    private func updatePreviousLevelNavigationOption(
        onlyOnDisabled: Bool = false
    ) {
        if onlyOnDisabled &&
            levelDataSource.levelWithIDExists(id: currentLevelID - 1) { return }
        
        gameView.updatePreviousLevelOption(
            disabled: !levelDataSource.levelWithIDExists(id: currentLevelID - 1)
        )
    }
    
    private func updateNextLevelNavigationOption(
        onlyOnDisabled: Bool = false
    ) {
        if onlyOnDisabled &&
            levelDataSource.levelWithIDExists(id: currentLevelID + 1) { return }
        
        gameView.updateNextLevelOption(
            disabled: !levelDataSource.levelWithIDExists(id: currentLevelID + 1)
        )
    }
    
    private func updateLastLevelPlayed() {
        let defaults = UserDefaults.standard
        defaults.set(currentLevelID, forKey: "lastLevel")
    }
    
    private func setCurrentLevelToLastPlayed() {
        let defaults = UserDefaults.standard
        currentLevelID = max(defaults.integer(forKey: "lastLevel"),1) - 1
    }
    
    private func setCompletedLevels() {
        let defaults = UserDefaults.standard
        guard let completed = defaults.string(forKey: "completed") else { return }
        completed.components(separatedBy: ",").forEach({ level in
            if let levelAsInt = Int(level) {
                completedLevels.insert(levelAsInt)
            }
        })
    }
    
    private func updateCompletedLevels() {
        let defaults = UserDefaults.standard
        var completedString = ""
        completedLevels.forEach({ level in
            completedString += String(level) + ","
        })
        
        defaults.set(completedString, forKey: "completed")
    }
    
    private func hasCompletedTutorialLevel() -> Bool {
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: "firstLevelCompleted")
    }
    
    private func markTutorialCompleted() {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "firstLevelCompleted")
    }
    
    private func updateInitialGameState() {
        if !hasCompletedTutorialLevel() {
            guard let solution = levelDataSource
                .solutionForLevel(levelID: currentLevelID) else { return }
            gameState = .TutorialInactive(solution: solution,index: 0)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
