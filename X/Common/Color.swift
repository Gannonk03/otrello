//
//  Color.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc static func hex(_ value: UInt32) -> UIColor {
      let r = CGFloat((value & 0xFF0000) >> 16) / 255.0
      let g = CGFloat((value & 0xFF00) >> 8) / 255.0
      let b = CGFloat(value & 0xFF) / 255.0

      return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
    func hueify(brightnessFactor: CGFloat = 0.7) -> UIColor {
        var h: CGFloat = 0.0
        var s: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 0.0
        
        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
            return UIColor(
                hue: h,
                saturation: s,
                brightness: b * brightnessFactor,
                alpha: a
            )
        }
        
        return .black
    }
}

class Color {    
    static func moverColor(color: BlockColor) -> UIColor {
        switch color {
        case .Red:
            return UIColor(
                red:0.847,
                green:0.376,
                blue:0.376,
                alpha:1
            ).hueify(brightnessFactor: 1.3)
        case .White:
            return .white
        }
    }
    
    static var transitionTextColor: UIColor {
        UIColor.hex(0x1EB980)
        //return UIColor(red:0.925, green:0.941, blue:0.945, alpha:1)
    }
    
    static var borderOutline: UIColor {
        UIColor.hex(0x9fa8da).withAlphaComponent(0.7)
    }
    
    static var emptyTileColor: UIColor {
        let base = UIColor(red:18/255, green:20/255, blue:27/255, alpha:1)
        return base.hueify(brightnessFactor: 0.9)
    }
    
    static var backgroundColor: UIColor {
        let base = UIColor(red:18/255, green:20/255, blue:23/255, alpha:1)
        return base.hueify(brightnessFactor: 0.8)
    }
    
    static var dividerColor: UIColor {
        UIColor.hex(0xa094b7).hueify(brightnessFactor: 0.3)
    }
}
