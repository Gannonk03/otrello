//
//  Queue.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class QueueItem<T> {
    let value: T!
    var next: QueueItem?
    
    init(newvalue: T?) {
        self.value = newvalue
    }
}

struct Queue<T> {
    var first: QueueItem<T>?
    var last: QueueItem<T>?
    
    mutating func enqueue(value: T){
        let current = last
        last = QueueItem(newvalue: value)
        
        if(current == nil) { first = last }
        else { current!.next = last }
    }
    
    func isEmpty() -> Bool {
        return first == nil
    }
    
    mutating func dequeue() -> T? {
        if(first == nil) { return nil }
        let item = first!.value
        first = first?.next
        if(first == nil) { last = nil }
        return item
    }
}
