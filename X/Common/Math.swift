//
//  Math.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

func roundOff(value: CGFloat) -> CGFloat {
    let rounded = abs(floor(value) - value) < abs(ceil(value) - value)
        ? floor(value)
        : ceil(value)
    
    return rounded
}

extension Double {
    var degreesToRadians: Double { return self * .pi / 180 }
    var radiansToDegrees: Double { return self * 180 / .pi }
    var radiansToDegreesNormalized: Double {
        var degrees = self * 180 / .pi
        if degrees < 0.0 { degrees = degrees + 360.0 }
        return degrees
    }
}
