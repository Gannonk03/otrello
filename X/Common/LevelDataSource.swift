//
//  LevelTextures.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

struct LevelDataSource {
    private var levelCache = Dictionary<Int,Level>()
    private var solutions = Dictionary<Int,[Direction]>()
    
    private let boardData: GameBoardData
    let maxLevel = 30
    
    init(viewWidth: CGFloat) {
        let padding: CGFloat = 2.0
        let maxCols = GameModel.maxCols, maxRows = GameModel.maxRows
        var boardWidth = viewWidth - 40.0
        
        let p = CGFloat(maxCols + 1) * padding
        let p1 = CGFloat(maxRows + 1) * padding
        let width = roundOff(value: (boardWidth - p) / CGFloat(maxCols))
        
        boardWidth = width * CGFloat(maxCols) + p
        
        let boardHeight = width * CGFloat(maxRows) + p1
        
        var textures = Dictionary<TextureType, SKTexture?>()
                
        let colors: Array<BlockColor> = [.Red,.White]
        
        for color in colors {
            let tileView = MoverTileView(
                length: width - 8.0,
                radius: 3.0,
                blockColor: color
            )
            
            let texture = SKTexture(image: tileView.takeSnapshot())
            
            textures[TextureType(rawValue: color.rawValue)!] = texture
        }
        
        let blockerView = BlockerView(
            color: Color.borderOutline,
            length: width ,
            radius: 3.0
        )
        
        let tailRed = MoverTileView(length: width - 8.0, radius: 3.0, blockColor: .Red)
        let tailWhite = MoverTileView(length: width - 8.0, radius: 3.0, blockColor: .White)
        
        textures[.TailRed] = SKTexture(image: tailRed.takeSnapshot())
        textures[.TailWhite] = SKTexture(image: tailWhite.takeSnapshot())
        textures[.Blocker] = SKTexture(image: blockerView.takeSnapshot())

        let emptyTile = EmptyTileView(
            color: UIColor.white,
            length: width
        )

        textures[.Empty] = SKTexture(image: emptyTile.takeSnapshot())
        
        emptyTile.layer.cornerRadius = 3.0
        emptyTile.layer.maskedCorners = [.layerMinXMinYCorner]
        
        textures[.BottomLeftCorner] = SKTexture(image: emptyTile.takeSnapshot())
        
        emptyTile.layer.maskedCorners = [.layerMinXMaxYCorner]
        
        textures[.TopLeftCorner] = SKTexture(image: emptyTile.takeSnapshot())
        
        emptyTile.layer.maskedCorners = [.layerMaxXMaxYCorner]
        
        textures[.TopRightCorner] = SKTexture(image: emptyTile.takeSnapshot())
        
        emptyTile.layer.maskedCorners = [.layerMaxXMinYCorner]
        
        textures[.BottomRightCorner] = SKTexture(image: emptyTile.takeSnapshot())
        
        emptyTile.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        textures[.TopRightBottomRightCorner] = SKTexture(image: emptyTile.takeSnapshot())

        emptyTile.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        textures[.TopLeftBottomLeftCorner] = SKTexture(image: emptyTile.takeSnapshot())
        
        emptyTile.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]

        textures[.TopLeftTopRightCorner] = SKTexture(image: emptyTile.takeSnapshot())
        
        emptyTile.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        textures[.BottomLeftBottomRightCorner] = SKTexture(image: emptyTile.takeSnapshot())
    
        textures[.NotInPlay] = nil
  
        textures[.bgTypeA] = SKTexture(linearGradientWithAngle: 90 * CGFloat.pi / 180, colors: [
            UIColor.hex(0x91358e),
            UIColor.hex(0x121b6c)
        ],
        locations: [0,1.0],
        size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        textures[.bgTypeB] = SKTexture(linearGradientWithAngle: 90 * CGFloat.pi / 180, colors: [
            UIColor.hex(0xc34655),
            UIColor.hex(0x041142)
        ],
        locations: [0,1.0],
        size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        textures[.bgTypeC] = SKTexture(linearGradientWithAngle: 90 * CGFloat.pi / 180, colors: [
            UIColor.hex(0x347264),
            UIColor.hex(0x041b2d)
        ],
        locations: [0,1.0],
        size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        let starView = Star(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        
        textures[.Star] = SKTexture(image: starView.takeSnapshot())
        
        boardData = GameBoardData(
            boardWidth: boardWidth,
            boardHeight: boardHeight,
            tileWidth: width,
            padding: padding,
            textures: textures
        )
        
        addSolutions()
    }
    
    func levelWithIDExists(id: Int) -> Bool {
        if id < 1 { return false }
        return levelCache[id] != nil
    }
    
    func solutionForLevel(levelID: Int) -> [Direction]? {
        return solutions[levelID]
    }
    
    mutating func getLevel(levelID: Int) -> Level? {
        let id = levelID > maxLevel ? 1 : levelID
        let path = Bundle.main.path(forResource: "Levels", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        
        for i in id-1...id+1 {
            if levelCache[i] != nil { continue }
            
            if let levelData = dict?.value(forKey: "\(i)") as? Dictionary<String,AnyObject> {
                let rows = levelData["rows"] as! Int
                let cols = levelData["cols"] as! Int
                var board = Array<Tile>(repeating: NotInPlay(), count: rows * cols)
                var board2d = Array<[Tile]>(
                    repeating: Array<Tile>(repeating: NotInPlay(), count: cols),
                    count: rows
                )
                
                for properties in levelData["board"] as! Array<Dictionary<String,AnyObject>> {
                    let type = TileType(rawValue: properties["Type"]! as! String)!
                    let boardIndex = properties["Index"] as! Int
                    let row = boardIndex / cols
                    let col = boardIndex % cols
                    switch type {
                    case .Mover:
                        let color = BlockColor(rawValue: properties["Color"]! as! String)!
                        board[boardIndex] = Mover(blockColor: color)
                        board2d[row][col] = Mover(blockColor: color)
                    case .Blocker:
                        board[boardIndex] = Blocker()
                        board2d[row][col] = Blocker()
                    case .Empty:
                        board[boardIndex] = Empty()
                        board2d[row][col] = Empty()
                    default:
                        break
                    }
                }
                
                var corners = Array<CornerProps>(
                    repeating: CornerProps(topLeft: false, topRight: false, bottomLeft: false, bottomRight: false),
                    count: rows * cols
                )
                
                for row in 0..<rows {
                    for col in 0..<cols {
                        if board2d[row][col].type == .NotInPlay { continue }
                        var topLeft = false
                        var topRight = false
                        var bottomLeft = false
                        var bottomRight = false
            
                        if
                        (!isValid(row: row, col: col-1, maxRow: rows, maxCol: cols) || board2d[row][col-1].type == .NotInPlay) &&
                        (!isValid(row: row-1, col: col, maxRow: rows, maxCol: cols) || board2d[row-1][col].type == .NotInPlay) {
                            bottomLeft = true
                        }
                        
                        if
                        (!isValid(row: row, col: col-1, maxRow: rows, maxCol: cols) || board2d[row][col-1].type == .NotInPlay) &&
                        (!isValid(row: row+1, col: col, maxRow: rows, maxCol: cols) || board2d[row+1][col].type == .NotInPlay) {
                            topLeft = true
                        }
                        
                        if
                       (!isValid(row: row, col: col+1, maxRow: rows, maxCol: cols) || board2d[row][col+1].type == .NotInPlay) &&
                       (!isValid(row: row+1, col: col, maxRow: rows, maxCol: cols) || board2d[row+1][col].type == .NotInPlay) {
                           topRight = true
                       }
                        
                        if
                        (!isValid(row: row, col: col+1, maxRow: rows, maxCol: cols) || board2d[row][col+1].type == .NotInPlay) &&
                        (!isValid(row: row-1, col: col, maxRow: rows, maxCol: cols) || board2d[row-1][col].type == .NotInPlay) {
                            bottomRight = true
                        }
                        let idx = row * cols + col
                        corners[idx] = CornerProps(
                            topLeft: topLeft,
                            topRight: topRight,
                            bottomLeft: bottomLeft,
                            bottomRight: bottomRight
                        )
                    }
                }
                let bgTextureType: TextureType = i / 10 == 0 ? .bgTypeA : (i / 10 == 1 ? .bgTypeB : .bgTypeC)

                levelCache[i] = Level(
                    id: i,
                    rows: rows,
                    cols: cols,
                    board: board,
                    corners: corners,
                    bgTextureType: bgTextureType,
                    boardData: boardData
                )
            }
        }
        
        return levelCache[id]
    }
    
    private func isValid(row: Int, col: Int, maxRow: Int, maxCol: Int) -> Bool {
        return row >= 0 && row < maxRow && col >= 0 && col < maxCol
    }
    
    private mutating func addSolutions() {
        solutions[1] = [.Right, .Up]
    }
}
