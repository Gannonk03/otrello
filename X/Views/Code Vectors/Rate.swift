//
//  Rate.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class Rate: UIView {
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 54, height: 51))
        
        backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {
        let fillColor = UIColor.white
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 51.6, y: 18))
        bezierPath.addLine(to: CGPoint(x: 34.8, y: 16.6))
        bezierPath.addLine(to: CGPoint(x: 28.3, y: 1.1))
        bezierPath.addCurve(
            to: CGPoint(x: 26.6, y: 0),
            controlPoint1: CGPoint(x: 28, y: 0.4),
            controlPoint2: CGPoint(x: 27.3, y: 0)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 24.9, y: 1.1),
            controlPoint1: CGPoint(x: 25.9, y: 0),
            controlPoint2: CGPoint(x: 25.2, y: 0.4)
        )
        bezierPath.addLine(to: CGPoint(x: 18.4, y: 16.6))
        bezierPath.addLine(to: CGPoint(x: 1.6, y: 18))
        bezierPath.addCurve(
            to: CGPoint(x: 0, y: 19.3),
            controlPoint1: CGPoint(x: 0.9, y: 18.1),
            controlPoint2: CGPoint(x: 0.2, y: 18.6)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 0.6, y: 21.3),
            controlPoint1: CGPoint(x: -0.2, y: 20),
            controlPoint2: CGPoint(x: 0, y: 20.8)
        )
        bezierPath.addLine(to: CGPoint(x: 13.3, y: 32.3))
        bezierPath.addLine(to: CGPoint(x: 9.5, y: 48.7))
        bezierPath.addCurve(
            to: CGPoint(x: 10.2, y: 50.6),
            controlPoint1: CGPoint(x: 9.3, y: 49.4),
            controlPoint2: CGPoint(x: 9.6, y: 50.2)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 12.2, y: 50.7),
            controlPoint1: CGPoint(x: 10.8, y: 51),
            controlPoint2: CGPoint(x: 11.6, y: 51.1)
        )
        bezierPath.addLine(to: CGPoint(x: 26.6, y: 42))
        bezierPath.addLine(to: CGPoint(x: 41, y: 50.7))
        bezierPath.addCurve(
            to: CGPoint(x: 42, y: 51),
            controlPoint1: CGPoint(x: 41.3, y: 50.9),
            controlPoint2: CGPoint(x: 41.6, y: 51)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 43.1, y: 50.7),
            controlPoint1: CGPoint(x: 42.4, y: 51),
            controlPoint2: CGPoint(x: 42.8, y: 50.9)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 43.8, y: 48.8),
            controlPoint1: CGPoint(x: 43.7, y: 50.3),
            controlPoint2: CGPoint(x: 44, y: 49.5)
        )
        bezierPath.addLine(to: CGPoint(x: 40, y: 32.4))
        bezierPath.addLine(to: CGPoint(x: 52.7, y: 21.4))
        bezierPath.addCurve(
            to: CGPoint(x: 53.3, y: 19.4),
            controlPoint1: CGPoint(x: 53.3, y: 20.9),
            controlPoint2: CGPoint(x: 53.5, y: 20.1)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 51.6, y: 18),
            controlPoint1: CGPoint(x: 53, y: 18.6),
            controlPoint2: CGPoint(x: 52.3, y: 18.1)
        )
        bezierPath.close()
        bezierPath.move(to: CGPoint(x: 36.6, y: 30.2))
        bezierPath.addCurve(
            to: CGPoint(x: 36, y: 32),
            controlPoint1: CGPoint(x: 36.1, y: 30.7),
            controlPoint2: CGPoint(x: 35.9, y: 31.3)
        )
        bezierPath.addLine(to: CGPoint(x: 39.1, y: 45.2))
        bezierPath.addLine(to: CGPoint(x: 27.5, y: 38.2))
        bezierPath.addCurve(
            to: CGPoint(x: 26.5, y: 37.9),
            controlPoint1: CGPoint(x: 27.2, y: 38),
            controlPoint2: CGPoint(x: 26.9, y: 37.9)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 25.5, y: 38.2),
            controlPoint1: CGPoint(x: 26.2, y: 37.9),
            controlPoint2: CGPoint(x: 25.8, y: 38)
        )
        bezierPath.addLine(to: CGPoint(x: 13.9, y: 45.2))
        bezierPath.addLine(to: CGPoint(x: 17, y: 32))
        bezierPath.addCurve(
            to: CGPoint(x: 16.4, y: 30.2),
            controlPoint1: CGPoint(x: 17.2, y: 31.3),
            controlPoint2: CGPoint(x: 16.9, y: 30.6)
        )
        bezierPath.addLine(to: CGPoint(x: 6.1, y: 21.3))
        bezierPath.addLine(to: CGPoint(x: 19.6, y: 20.2))
        bezierPath.addCurve(
            to: CGPoint(x: 21.1, y: 19.1),
            controlPoint1: CGPoint(x: 20.3, y: 20.1),
            controlPoint2: CGPoint(x: 20.9, y: 19.7)
        )
        bezierPath.addLine(to: CGPoint(x: 26.4, y: 6.6))
        bezierPath.addLine(to: CGPoint(x: 31.7, y: 19.1))
        bezierPath.addCurve(
            to: CGPoint(x: 33.2, y: 20.2),
            controlPoint1: CGPoint(x: 32, y: 19.7),
            controlPoint2: CGPoint(x: 32.6, y: 20.2)
        )
        bezierPath.addLine(to: CGPoint(x: 46.7, y: 21.3))
        bezierPath.addLine(to: CGPoint(x: 36.6, y: 30.2))
        bezierPath.close()
        bezierPath.miterLimit = 4;
        
        fillColor.setFill()
        bezierPath.fill()
    }
}
