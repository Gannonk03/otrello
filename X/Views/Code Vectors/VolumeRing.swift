//
//  OuterVolumeRing.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

enum VolumeRingStyle {
    case Inner
    case Middle
    case Outer
}

class VolumeRing: UIView {    
    private var currentColor: UIColor
    private var ringPath = UIBezierPath()
    
    private let activeColor = UIColor.white
    private let inactiveColor = UIColor.init(white: 1, alpha: 0.3)
    
    private var outerRingPath: UIBezierPath {
        let bezierPath = UIBezierPath()
        
        bezierPath.move(to: CGPoint(x: 2.8, y: 0.1))
        bezierPath.addLine(to: CGPoint(x: 0, y: 2.9))
        bezierPath.addCurve(
            to: CGPoint(x: 0, y: 40.2),
            controlPoint1: CGPoint(x: 10.4, y: 13.2),
            controlPoint2: CGPoint(x: 10.4, y: 29.9)
        )
        bezierPath.addLine(to: CGPoint(x: 2.8, y: 43))
        bezierPath.addCurve(
            to: CGPoint(x: 3.1, y: 43),
            controlPoint1: CGPoint(x: 2.9, y: 43.1),
            controlPoint2: CGPoint(x: 3, y: 43.1)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 3.1, y: 0.1),
            controlPoint1: CGPoint(x: 14.9, y: 31.1),
            controlPoint2: CGPoint(x: 14.9, y: 12)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 2.8, y: 0.1),
            controlPoint1: CGPoint(x: 3, y: 0),
            controlPoint2: CGPoint(x: 2.9, y: 0)
        )
        bezierPath.close()
        bezierPath.miterLimit = 4
        
        return bezierPath
    }
    
    private var innerRingPath: UIBezierPath {
        let bezierPath = UIBezierPath()
        
        bezierPath.move(to: CGPoint(x: 2, y: 0.5))
        bezierPath.addCurve(
            to: CGPoint(x: 0.2, y: 2.3),
            controlPoint1: CGPoint(x: 1.5, y: 1),
            controlPoint2: CGPoint(x: 0.9, y: 1.6)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 0.2, y: 19.7),
            controlPoint1: CGPoint(x: 5, y: 7.1),
            controlPoint2: CGPoint(x: 5, y: 14.9)
        )
        bezierPath.addLine(to: CGPoint(x: 2, y: 21.5))
        bezierPath.addCurve(
            to: CGPoint(x: 4.1, y: 21.4),
            controlPoint1: CGPoint(x: 2.6, y: 22.1),
            controlPoint2: CGPoint(x: 3.6, y: 22)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 4.1, y: 0.7),
            controlPoint1: CGPoint(x: 9.1, y: 15.4),
            controlPoint2: CGPoint(x: 9.1, y: 6.7)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 2, y: 0.5),
            controlPoint1: CGPoint(x: 3.6, y: 0),
            controlPoint2: CGPoint(x: 2.6, y: 0)
        )
        bezierPath.close()
        bezierPath.miterLimit = 4
        
        return bezierPath
    }
    
    private var middleRingPath: UIBezierPath {
        let bezierPath = UIBezierPath()
        
        bezierPath.move(to: CGPoint(x: 2.3, y: 0.3))
        bezierPath.addLine(to: CGPoint(x: -0.1, y: 2.7))
        bezierPath.addCurve(
            to: CGPoint(x: -0.1, y: 30.4),
            controlPoint1: CGPoint(x: 7.6, y: 10.3),
            controlPoint2: CGPoint(x: 7.6, y: 22.8)
        )
        bezierPath.addLine(to: CGPoint(x: 2.3, y: 32.8))
        bezierPath.addCurve(
            to: CGPoint(x: 3.5, y: 32.8),
            controlPoint1: CGPoint(x: 2.6, y: 33.1),
            controlPoint2: CGPoint(x: 3.2, y: 33.1)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 3.5, y: 0.3),
            controlPoint1: CGPoint(x: 12.1, y: 23.7),
            controlPoint2: CGPoint(x: 12.1, y: 9.5)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 2.3, y: 0.3),
            controlPoint1: CGPoint(x: 3.2, y: -0.1),
            controlPoint2: CGPoint(x: 2.7, y: -0.1)
        )
        bezierPath.close()
        bezierPath.miterLimit = 4
        
        return bezierPath
    }
    
    init(ringStyle: VolumeRingStyle) {
        self.currentColor = activeColor
        
        super.init(frame: .zero)
        
        backgroundColor = UIColor.clear
        
        switch ringStyle {
        case .Outer:
            ringPath = outerRingPath
            frame = CGRect(x: 0, y: 0, width: 12.0, height: 43.0)
        case .Middle:
            ringPath = middleRingPath
            frame = CGRect(x: 0, y: 0, width: 10.0, height: 33.0)
        case .Inner:
            ringPath = innerRingPath
            frame = CGRect(x: 0, y: 0, width: 8.0, height: 23.0)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setActive() {
        currentColor = activeColor
        
        setNeedsDisplay()
    }
    
    func setInactive() {
        currentColor = inactiveColor
        
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        let fillColor = currentColor
        
        fillColor.setFill()
        ringPath.fill()
    }
}
