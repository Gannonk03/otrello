//
//  Arrow.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class Arrow: UIView {
    
    private let fill: UIColor

    init(fillColor: UIColor = UIColor.white) {
        
        self.fill = fillColor
        super.init(frame: CGRect(x: 0, y: 0, width: 25.0, height: 43.0))
        
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        let bezierPath = UIBezierPath()
        
        bezierPath.move(to: CGPoint(x: 24.3, y: 20.7))
        bezierPath.addLine(to: CGPoint(x: 4.3, y: 0.7))
        bezierPath.addCurve(
            to: CGPoint(x: 2.9, y: 0.7),
            controlPoint1: CGPoint(x: 3.9, y: 0.3),
            controlPoint2: CGPoint(x: 3.3, y: 0.3)
        )
        bezierPath.addLine(to: CGPoint(x: 1, y: 2.6))
        bezierPath.addCurve(
            to: CGPoint(x: 0.7, y: 3.3),
            controlPoint1: CGPoint(x: 0.8, y: 2.8),
            controlPoint2: CGPoint(x: 0.7, y: 3.1)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 1, y: 4),
            controlPoint1: CGPoint(x: 0.7, y: 3.6),
            controlPoint2: CGPoint(x: 0.8, y: 3.8)
        )
        bezierPath.addLine(to: CGPoint(x: 18.4, y: 21.4))
        bezierPath.addLine(to: CGPoint(x: 1, y: 38.8))
        bezierPath.addCurve(
            to: CGPoint(x: 0.7, y: 39.5),
            controlPoint1: CGPoint(x: 0.8, y: 39),
            controlPoint2: CGPoint(x: 0.7, y: 39.3)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 1, y: 40.2),
            controlPoint1: CGPoint(x: 0.7, y: 39.8),
            controlPoint2: CGPoint(x: 0.8, y: 40)
        )
        bezierPath.addLine(to: CGPoint(x: 2.9, y: 42.1))
        bezierPath.addCurve(
            to: CGPoint(x: 3.6, y: 42.4),
            controlPoint1: CGPoint(x: 3.1, y: 42.3),
            controlPoint2: CGPoint(x: 3.4, y: 42.4)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 4.3, y: 42.1),
            controlPoint1: CGPoint(x: 3.9, y: 42.4),
            controlPoint2: CGPoint(x: 4.1, y: 42.3)
        )
        bezierPath.addLine(to: CGPoint(x: 24.3, y: 22.1))
        bezierPath.addCurve(
            to: CGPoint(x: 24.3, y: 20.7),
            controlPoint1: CGPoint(x: 24.7, y: 21.8),
            controlPoint2: CGPoint(x: 24.7, y: 21.1)
        )
        bezierPath.close()
        bezierPath.miterLimit = 4;
        
        fill.setFill()
        bezierPath.fill()
    }
}
