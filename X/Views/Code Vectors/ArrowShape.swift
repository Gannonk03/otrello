//
//  TileShapeNode.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class ArrowShape: UIView {
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 40, height: 59.0))
        
        backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        let fillColor = UIColor.white
        let bezierPath = UIBezierPath()
        
        bezierPath.move(to: CGPoint(x: 37.4, y: 26.21))
        bezierPath.addLine(to: CGPoint(x: 7.22, y: 1.27))
        bezierPath.addCurve(
            to: CGPoint(x: 3.18, y: 0.61),
            controlPoint1: CGPoint(x: 6.16, y: 0.4),
            controlPoint2: CGPoint(x: 4.57, y: 0.14)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 0.9, y: 3.44),
            controlPoint1: CGPoint(x: 1.8, y: 1.08),
            controlPoint2: CGPoint(x: 0.9, y: 2.2)
        )
        bezierPath.addLine(to: CGPoint(x: 16.73, y: 28.38))
        bezierPath.addLine(to: CGPoint(x: 0.9, y: 53.32))
        bezierPath.addCurve(
            to: CGPoint(x: 3.18, y: 56.14),
            controlPoint1: CGPoint(x: 0.9, y: 54.56),
            controlPoint2: CGPoint(x: 1.8, y: 55.67)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 4.6, y: 56.38),
            controlPoint1: CGPoint(x: 3.64, y: 56.3),
            controlPoint2: CGPoint(x: 4.13, y: 56.38)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 7.22, y: 55.48),
            controlPoint1: CGPoint(x: 5.57, y: 56.38),
            controlPoint2: CGPoint(x: 6.51, y: 56.07)
        )
        bezierPath.addLine(to: CGPoint(x: 37.4, y: 30.54))
        bezierPath.addCurve(
            to: CGPoint(x: 38.49, y: 28.38),
            controlPoint1: CGPoint(x: 38.09, y: 29.97),
            controlPoint2: CGPoint(x: 38.49, y: 29.18)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 37.4, y: 26.21),
            controlPoint1: CGPoint(x: 38.49, y: 27.57),
            controlPoint2: CGPoint(x: 38.09, y: 26.78)
        )
        
        bezierPath.close()
        fillColor.setFill()
        bezierPath.fill()
    }
}
