//
//  HelpView.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class Redo: UIView {    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 54, height: 51))
        
        backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {
        let fillColor = UIColor.white
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 53.1, y: 36.3))
        bezierPath.addCurve(
            to: CGPoint(x: 50.5, y: 35),
            controlPoint1: CGPoint(x: 52.8, y: 35.2),
            controlPoint2: CGPoint(x: 51.6, y: 34.6)
        )
        bezierPath.addLine(to: CGPoint(x: 47.8, y: 35.9))
        bezierPath.addCurve(
            to: CGPoint(x: 50.3, y: 25),
            controlPoint1: CGPoint(x: 49.4, y: 32.5),
            controlPoint2: CGPoint(x: 50.3, y: 28.8)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 25.3, y: 0),
            controlPoint1: CGPoint(x: 50.3, y: 11.2),
            controlPoint2: CGPoint(x: 39.1, y: 0)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 0.3, y: 25),
            controlPoint1: CGPoint(x: 11.5, y: 0),
            controlPoint2: CGPoint(x: 0.3, y: 11.2)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 25.3, y: 50),
            controlPoint1: CGPoint(x: 0.3, y: 38.8),
            controlPoint2: CGPoint(x: 11.5, y: 50)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 27.3, y: 48),
            controlPoint1: CGPoint(x: 26.4, y: 50),
            controlPoint2: CGPoint(x: 27.3, y: 49.1)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 25.3, y: 46),
            controlPoint1: CGPoint(x: 27.3, y: 46.9),
            controlPoint2: CGPoint(x: 26.4, y: 46)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 4.4, y: 25.1),
            controlPoint1: CGPoint(x: 13.8, y: 46),
            controlPoint2: CGPoint(x: 4.4, y: 36.6)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 25.3, y: 4.2),
            controlPoint1: CGPoint(x: 4.4, y: 13.6),
            controlPoint2: CGPoint(x: 13.8, y: 4.2)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 46.2, y: 25.1),
            controlPoint1: CGPoint(x: 36.8, y: 4.2),
            controlPoint2: CGPoint(x: 46.2, y: 13.6)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 44.1, y: 34.2),
            controlPoint1: CGPoint(x: 46.2, y: 28.3),
            controlPoint2: CGPoint(x: 45.5, y: 31.4)
        )
        bezierPath.addLine(to: CGPoint(x: 43.2, y: 31.4))
        bezierPath.addCurve(
            to: CGPoint(x: 40.6, y: 30.1),
            controlPoint1: CGPoint(x: 42.9, y: 30.3),
            controlPoint2: CGPoint(x: 41.7, y: 29.7)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 39.3, y: 32.7),
            controlPoint1: CGPoint(x: 39.5, y: 30.4),
            controlPoint2: CGPoint(x: 38.9, y: 31.6)
        )
        bezierPath.addLine(to: CGPoint(x: 41.7, y: 40.1))
        bezierPath.addCurve(
            to: CGPoint(x: 43.6, y: 41.5),
            controlPoint1: CGPoint(x: 42, y: 41),
            controlPoint2: CGPoint(x: 42.8, y: 41.5)
        )
        bezierPath.addCurve(
            to: CGPoint(x: 44.2, y: 41.4),
            controlPoint1: CGPoint(x: 43.8, y: 41.5),
            controlPoint2: CGPoint(x: 44, y: 41.5)
        )
        bezierPath.addLine(to: CGPoint(x: 51.6, y: 39))
        bezierPath.addCurve(
            to: CGPoint(x: 53.1, y: 36.3),
            controlPoint1: CGPoint(x: 52.8, y: 38.6),
            controlPoint2: CGPoint(x: 53.4, y: 37.4)
        )
        bezierPath.close()
        bezierPath.miterLimit = 4;
        
        fillColor.setFill()
        bezierPath.fill()
    }
}
