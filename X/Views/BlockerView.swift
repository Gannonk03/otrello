//
//  BlockerView.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class BlockerView: UIView {
    init(color: UIColor, length: CGFloat, radius: CGFloat) {
        super.init(frame: CGRect(x: 0, y: 0, width: length, height: length))
    
        backgroundColor = .clear
        layer.cornerRadius = radius
        layer.borderWidth = 2.5
        layer.borderColor = color.cgColor
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
