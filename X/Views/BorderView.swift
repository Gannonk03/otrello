//
//  BorderView.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class BorderView: UIView {
    init(length: CGFloat, radius: CGFloat,
         boardWidth: CGFloat, boardRadius: CGFloat) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: length, height: length))
        
        layer.cornerRadius = radius
        backgroundColor = Color.borderOutline
        
        addTopLayer(radius: radius)
        addPlayableLayer(length: boardWidth, radius: boardRadius)
    }
    
    private func addTopLayer(radius: CGFloat) {
        var frame = self.bounds
        frame.size.height -= 5.0
        frame.size.width -= 5.0
        
        let topLayer = UIView(frame: frame)
        topLayer.backgroundColor = Color.emptyTileColor.hueify()
        topLayer.layer.cornerRadius = radius - 2.0
        
        topLayer.center = center
        
        addSubview(topLayer)
    }
    
    private func addPlayableLayer(length: CGFloat, radius: CGFloat) {
        let frame = CGRect(x: 0, y: 0, width: length, height: length)

        let topLayer = UIView(frame: frame)
        topLayer.backgroundColor = Color.dividerColor
        topLayer.layer.cornerRadius = radius
        
        topLayer.center = center
        
        addSubview(topLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
