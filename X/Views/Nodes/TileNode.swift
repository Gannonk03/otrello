//
//  TileNode.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

struct Stop: Equatable {
    var location: CGPoint
    var duration: Double
    var timingMode: SKActionTimingMode
    
    init(
        location: CGPoint,
        duration: Double,
        timingMode: SKActionTimingMode = .easeOut
    ) {
        self.location = location
        self.duration = duration
        self.timingMode = timingMode
    }
}

func ==(lhs: Stop, rhs: Stop) -> Bool {
    return
        lhs.location == rhs.location &&
        lhs.duration == rhs.duration
}

enum TileName: String {
    case Red
    case White
    case RedWithTrail
    case WhiteWithTrail
    
    static func getTrailName(tileName: TileName) -> String {
        switch tileName {
        case .Red, .RedWithTrail:
            return "RedWithTrail"
        case .White, .WhiteWithTrail:
            return "WhiteWithTrail"
        }
    }
}

protocol TileNodeProtocol {
    var node: SKSpriteNode { get }
    var startingCoordinate: BoardCoordinate { get }
}

struct TileNode {
    var node: SKSpriteNode
    var startingCoordinate: BoardCoordinate
    var startingTexture: SKTexture?
    
    init(startingCoordinate: BoardCoordinate, texture: SKTexture?) {
        self.startingCoordinate = startingCoordinate
        self.startingTexture = texture
        
        node = SKSpriteNode(texture: texture)
    }
}

