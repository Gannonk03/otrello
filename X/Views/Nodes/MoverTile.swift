//
//  MoverTile.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

protocol MoverTileProtocol: TileNodeProtocol {
    func resetToPosition(position: CGPoint)
    func move(stop: Stop, withTrail: Bool, completion: (() -> Void)!)
    func flip(texture: SKTexture, name: TileName)
}

struct MoverTile: MoverTileProtocol {
    private(set) var node: SKSpriteNode
    private(set) var startingCoordinate: BoardCoordinate
    private(set) var startingName: TileName
    
    private var tile: TileNode
    
    init(tile: TileNode, name: TileName) {
        self.tile = tile
        self.startingCoordinate = tile.startingCoordinate
        self.startingName = name
        
        tile.node.zPosition = 4.0
        tile.node.name = name.rawValue
        
        node = tile.node
    }
    
    func move(stop: Stop, withTrail: Bool, completion: (() -> Void)!) {
        let moveAction = SKAction.move(
            to: stop.location,
            duration: stop.duration
        )
        
        moveAction.timingMode = stop.timingMode
        
        if !withTrail {
            node.run(moveAction,completion: completion)
            return
        }
        
        let lastName = node.name!
        
        node.name = TileName.getTrailName(
            tileName: TileName(rawValue: lastName)!
        )
        
        node.run(moveAction) {
            self.node.name = lastName
            completion()
        }
    }
    
    func flip(texture: SKTexture, name: TileName) {
        tile.node.name = name.rawValue
        
        let flipNode = SKSpriteNode(texture: texture)
        
        flipNode.zPosition = 5.0
        flipNode.setScale(0.0)
        
        let spreadAction = SKAction.scale(to: 1.1, duration: 0.13)
        let shrinkAction = SKAction.scale(to: 1.0, duration: 0.13)
        let popAction = SKAction.sequence([spreadAction,shrinkAction])
        
        popAction.timingMode = .easeOut

        node.addChild(flipNode)
        
        flipNode.run(popAction) {
            self.node.texture = nil
        }
    }
    
    func resetToPosition(position: CGPoint) {
        tile.node.removeAllChildren()
        tile.node.position = position
        tile.node.texture = tile.startingTexture
        tile.node.name = startingName.rawValue
    }
}
