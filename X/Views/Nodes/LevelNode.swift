//
//  LevelNode.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

class LevelNode : SKNode {
    var tiles: Dictionary<BoardCoordinate,TileNodeProtocol> = [:]
    var nodes = Set<SKNode>()
    var boardData: GameBoardData
    var level: Level
    var board: SKSpriteNode
    var markedNodes = Set<BoardCoordinate>()
    
    private let slideDuration = 0.2
    
    init(level: Level) {
        self.boardData = level.boardData
        self.level = level
    
        let bounds = UIScreen.main.bounds

        board = SKSpriteNode()
                
        super.init()
        var maxRow = 0
        var maxCol = 0
        for row in 0..<level.rows {
            for col in 0..<level.cols {
                let coordinate = BoardCoordinate(row: row, col: col)
                let idx = row * level.cols + col
                let type: TextureType = level.board[idx].type == .NotInPlay ? .NotInPlay : .Empty
                if type == .Empty && row + 1 > maxRow { maxRow = row + 1 }
                if type == .Empty && col + 1 > maxCol { maxCol = col + 1 }
                let tile = createTile(coordinate: coordinate, type: type.textureTypeWithCornerProps(level.corners[idx]))
                
                if type == .Empty {
                    let bgNode = SKSpriteNode(texture: level.boardData.textures[level.bgTextureType]!)
                    let cropNode = SKCropNode()
                    cropNode.addChild(bgNode)
                    cropNode.maskNode = SKSpriteNode(color: .white, size: tile.node.size)
                    tile.node.addChild(cropNode)
                }
                                
                board.addChild(tile.node)
                groupTile(coor: coordinate,tile: tile.node)
            }
        }
        let yDiff = CGFloat(GameModel.maxRows - maxRow)
        let yOffset = yDiff * (boardData.tileWidth + boardData.padding)
        
        let xDiff = CGFloat(GameModel.maxCols - maxCol)
        let xOffset = xDiff * (boardData.tileWidth + boardData.padding)
        
        let zeroX = bounds.size.width / 2.0
            - (boardData.boardWidth - xOffset) / 2.0
            + boardData.tileWidth / 2.0
        
        let zeroY = bounds.size.height / 2.0
            - (boardData.boardHeight - yOffset) / 2.0
            + boardData.tileWidth / 2.0
        
        board.position = CGPoint(x: zeroX,y: zeroY)
    
        addChild(board)
                
        board.zPosition = 2
        
        for child in board.children where !child.children.isEmpty {
            let xOffset = (child.position.x - (boardData.boardWidth - xOffset) / 2.0)
            let yOffset = (child.position.y - (boardData.boardHeight - yOffset) / 2.0)
            
            child.children.first!.children.first!.position = CGPoint(x: -xOffset, y: -yOffset)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(_ currentTime: TimeInterval) -> Bool {
        var levelComplete = true
        for child in board.children {
            if
                child.name == TileName.RedWithTrail.rawValue ||
                child.name == TileName.WhiteWithTrail.rawValue {
                
                let textureType: TextureType =
                    child.name == TileName.RedWithTrail.rawValue
                        ? .TailRed
                        : .TailWhite
                
                let trailNode = SKSpriteNode(
                    texture: boardData.textureFromType(type: textureType)
                )
                
                trailNode.position = child.position
                trailNode.zPosition = 4.0
                
                board.addChild(trailNode)

                let fadeAction = SKAction.fadeOut(withDuration: 0.075)
                let deleteAction = SKAction.run { [weak trailNode] in
                    trailNode?.removeFromParent()
                }
                
                trailNode.run(SKAction.sequence([fadeAction,deleteAction]))
            }
            if
                child.name == TileName.White.rawValue ||
                child.name == TileName.WhiteWithTrail.rawValue {
                levelComplete = false
            }
        }
        
        return levelComplete
    }
    
    func addTile(tile: Tile, coordinate: BoardCoordinate) {
        switch tile.type {
        case .Mover:
            guard let tile = tile as? Mover else { return }
            let textureType = TextureType(rawValue: tile.color.rawValue)!
            let mover = MoverTile(tile:
                createTile(
                    coordinate: coordinate,
                    type: textureType
                ),
                name: tile.color == .Red ? TileName.Red : TileName.White
            )
            tiles[coordinate] = mover
            groupTile(coor: coordinate, tile: mover.node)
            mover.node.position = getTilePosition(coordinate: coordinate)
            board.addChild(mover.node)
            
        case .Blocker:
            let blocker = BlockerTile(tile:
                createTile(coordinate: coordinate, type: .Blocker)
            )
            tiles[coordinate] = blocker
            groupTile(coor: coordinate, tile: blocker.node)
            blocker.node.position = getTilePosition(coordinate: coordinate)
            board.addChild(blocker.node)
        default:
            break
        }
    }
    
    private func getStop(
        fromKey: BoardCoordinate,
        toKey: BoardCoordinate
    ) -> Stop {
                    
        return Stop(
            location: getTilePosition(coordinate: toKey),
            duration: slideDuration
        )
    }
    
    func flipTile(fromKey: BoardCoordinate) {
        markedNodes.insert(fromKey)
    }
    
    func moveTile(
        fromKey: BoardCoordinate,
        toKey: BoardCoordinate,
        withTrail: Bool,
        completion: (() -> Void)! = nil
    ) {
        let tile = tiles[fromKey]! as! MoverTileProtocol
        
        tiles.removeValue(forKey: fromKey)
        tiles[toKey] = tile
        
        tile.move(
            stop: getStop(
            fromKey: fromKey,
            toKey: toKey
            ),
            withTrail: withTrail
        ) {
            if self.markedNodes.contains(toKey) {
                let tile = self.tiles[toKey] as! MoverTileProtocol
                let flipTexture = self.boardData.textureFromType(type: .Red)!
                
                tile.flip(texture: flipTexture, name: .Red)
                self.markedNodes.remove(toKey)
            }
            completion?()
        }
    }
    
    func resetTilesToStartingPositions() {        
        let values = tiles.values
        tiles.removeAll(keepingCapacity: true)
        
        for value in values {
            tiles[value.startingCoordinate] = value
        }
        
        for (index,_) in level.board.enumerated() {
            let row = index / level.cols
            let col = index % level.cols
            let coordinate = BoardCoordinate(row: row, col: col)
            
            if let tileProtocol = tiles[coordinate] as? MoverTileProtocol {
                tileProtocol.resetToPosition(
                    position: getTilePosition(coordinate: coordinate)
                )
            }
        }
    }
    
    private func createTile(
        coordinate: BoardCoordinate,
        type: TextureType
    ) -> TileNode {
        let tile = TileNode(
            startingCoordinate: coordinate,
            texture: boardData.textureFromType(type: type)
        )
        
        tile.node.position = getTilePosition(coordinate: coordinate)
        
        return tile
    }
    
    private func groupTile(coor: BoardCoordinate, tile: SKNode) {
        tile.alpha = 0.0
        nodes.insert(tile)
    }
    
    private func getTilePosition(coordinate: BoardCoordinate) -> CGPoint {
        return CGPoint(
            x: CGFloat(coordinate.col) * (boardData.tileWidth + boardData.padding),
            y: CGFloat(coordinate.row) * (boardData.tileWidth + boardData.padding)
        )
    }
}
