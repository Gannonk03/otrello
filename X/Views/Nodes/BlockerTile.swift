//
//  BlockerTile.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

struct BlockerTile: TileNodeProtocol {
    private(set) var node: SKSpriteNode
    private(set) var startingCoordinate: BoardCoordinate
    
    private var tile: TileNode
    
    init(tile: TileNode) {
        self.tile = tile
        self.startingCoordinate = tile.startingCoordinate
        
        tile.node.zPosition = 4.0
        
        node = tile.node
    }
}
