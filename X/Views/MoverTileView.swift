//
//  MoverTileView.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class MoverTileView: TileView {
    init(length: CGFloat, radius: CGFloat, blockColor: BlockColor) {
        super.init(
            color: Color.moverColor(color: blockColor),
            length: length,
            radius: radius
        )
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
