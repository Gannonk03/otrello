//
//  LevelNavigationButton.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

enum ArrowDirection {
    case Left
    case Right
}

protocol LevelNaviationDelegate: class {
    func previousLevelSelected()
    func nextLevelSelected()
}

class LevelNavigationButton: UIControl {    
    private let inactiveAlpha: CGFloat = 0.3
    private let activeAlpha: CGFloat = 0.7
    private let boundsPadding: CGFloat = 50.0
    
    private var disabled: Bool = false
    
    init(direction: ArrowDirection) {
        let arrow = Arrow()
        arrow.isUserInteractionEnabled = false
        
        var bounds = arrow.bounds
        bounds.size.width += boundsPadding
        bounds.size.height += boundsPadding
        
        super.init(frame: bounds)
        
        alpha = activeAlpha
        
        backgroundColor = UIColor.clear
        
        let rotation = direction == .Left ? CGFloat.pi : 0.0
        
        transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            .concatenating(CGAffineTransform(rotationAngle: rotation))
        
        addSubview(arrow)
        
        arrow.center = self.center
        
        setupTouchEvents()
    }
    
    func setDisabled(_ disabled: Bool, animated: Bool = false) {
        self.disabled = disabled
        self.updateState(animated: animated)
    }
    
    @objc func onTouch() {
        alpha = inactiveAlpha
    }
    
    @objc func onRelease() {
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            options: .allowUserInteraction,
            animations: { self.alpha = self.activeAlpha },
            completion: nil
        )
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Private
    private func setupTouchEvents() {
        addTarget(
            self,
            action: #selector(LevelNavigationButton.onTouch),
            for: UIControl.Event.touchDown
        )
        addTarget(
            self,
            action: #selector(LevelNavigationButton.onTouch),
            for: UIControl.Event.touchDragInside
        )

        addTarget(
            self,
            action: #selector(LevelNavigationButton.onRelease),
            for: UIControl.Event.touchDragExit
        )
        addTarget(
            self,
            action: #selector(LevelNavigationButton.onRelease),
            for: UIControl.Event.touchUpInside
        )
    }
    
    private func updateState(animated: Bool = false) {
        if disabled { setStateDisabled(animated) } else { setStateEnabled(animated) }
    }
    
    private func setStateEnabled(_ animated: Bool = false) {
        if !animated {
            alpha = activeAlpha
            isUserInteractionEnabled = true
            return
        }
        
        UIView.animate(withDuration: 1) {
            self.alpha = self.activeAlpha
        }
        
        isUserInteractionEnabled = true
    }
    
    private func setStateDisabled(_ animated: Bool = false ) {
        if !animated {
            alpha = inactiveAlpha
            isUserInteractionEnabled = false
            return
        }
       
        UIView.animate(withDuration: 1) {
            self.alpha = self.inactiveAlpha
        }
        
        isUserInteractionEnabled = false
    }
}
