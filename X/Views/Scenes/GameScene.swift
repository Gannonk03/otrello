//
//  GameScene.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import SpriteKit
import UIKit

class GameScene: SKScene {
    private var levels = Dictionary<LevelState,LevelNode>()
    private var bgNode: SKSpriteNode!
    private var bgNode2: SKSpriteNode!
    private var bgNode3: SKSpriteNode!
    private var swipeNode: SKSpriteNode!
    private var backgroundMusic: SKAudioNode!
    private var leftStar: SKSpriteNode!
    private var centerStar: SKSpriteNode!
    private var rightStar: SKSpriteNode!
    private let slide = SKAction.playSoundFileNamed("slide.wav", waitForCompletion: false)
    private let pop = SKAction.playSoundFileNamed("pop.wav", waitForCompletion: false)

    private let levelTextNode = SKLabelNode(
        fontNamed: "AvenirNextCondensed-MediumItalic"
    )
    
    private let startGameTextNode = SKLabelNode(
        fontNamed: "AvenirNextCondensed-MediumItalic"
    )
    
    weak var sceneDelegate: SceneProtocol!
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        setupGameBoard()
        setupLevelText()
        setupStars()
        setupStartGameText()
        
        setupMusic()
    }
    
    override func update(_ currentTime: TimeInterval) {
        if currentLevel().update(currentTime) {
            sceneDelegate.levelWon()
            swipeNode?.removeFromParent()
        }
    }
    
    func setupStars() {
        let textures = currentLevel().boardData.textures
        leftStar = SKSpriteNode(texture: textures[.Star]!)
        
        leftStar.position.y = view!.center.y * 1.85 - 35
        leftStar.position.x = view!.center.x - 25
        leftStar.alpha = 0.0
        
        rightStar = SKSpriteNode(texture: textures[.Star]!)
        
        rightStar.position.y = view!.center.y * 1.85 - 35
        rightStar.position.x = view!.center.x + 25
        rightStar.alpha = 0.0
        
        centerStar = SKSpriteNode(texture: textures[.Star]!)
        
        centerStar.position.y = view!.center.y * 1.85 - 45
        centerStar.position.x = view!.center.x
        centerStar.alpha = 0.0
        
        addChild(leftStar)
        addChild(rightStar)
        addChild(centerStar)
    }
    
    func setupMusic() {
        if let musicURL = Bundle.main.url(forResource: "zen", withExtension: "wav") {
            backgroundMusic = SKAudioNode(url: musicURL)
            addChild(backgroundMusic)
            backgroundMusic.run(SKAction.changeVolume(to: 0.2, duration: 0))
        }
    }
    
    func stopMusic() {
        backgroundMusic.removeFromParent()
    }
    
    func startMusic() {
        addChild(backgroundMusic)
    }
    
    func setupStartGameText() {
        startGameTextNode.verticalAlignmentMode = .center
        startGameTextNode.fontSize = 38
        startGameTextNode.text = "Tap to play"
        startGameTextNode.fontColor = .white
        startGameTextNode.position = CGPoint(x: view!.center.x, y: view!.center.y * 0.9)
        startGameTextNode.setScale(0.84)
        startGameTextNode.zPosition = 100
        
        let scaleDownAction = SKAction.scale(to: 0.92, duration: 0.7)
        let scaleDownMove = SKAction.moveBy(x: 0, y: 6, duration: 0.7)

        let scaleUpAction = SKAction.scale(to: 1.0, duration: 0.7)
        let scaleUpMove = SKAction.moveBy(x: 0, y: -6, duration: 0.7)
        
        let first = SKAction.group([scaleDownAction,scaleDownMove])
        let second = SKAction.group([scaleUpAction,scaleUpMove])
        
        addChild(startGameTextNode)
        startGameTextNode.run(SKAction.repeatForever(SKAction.sequence([second,first])))
    }
    
    func flipTile(at: (Int, Int)) {
        let fromKey = coordinateFromLocation(location: at)
        currentLevel().flipTile(fromKey: fromKey)
    }
    
    func playSlideSound() {
        self.removeAction(forKey: "slide")
        self.run(slide, withKey: "slide")
    }
    
    func playPopSound() {
        let wait = SKAction.wait(forDuration: 0.2)
        self.run(SKAction.sequence([wait,pop]), withKey: "pop")
    }
    
    func moveTile(
        from: (Int, Int),
        to: (Int, Int),
        withTrail: Bool,
        completion: (() -> Void)! = nil
    ) {
        
        let fromKey = coordinateFromLocation(location: from)
        let toKey = coordinateFromLocation(location: to)
        
        currentLevel().moveTile(
            fromKey: fromKey,
            toKey: toKey,
            withTrail: withTrail,
            completion: completion
        )
    }
    
    func restartLevel() {
        self.currentLevel().resetTilesToStartingPositions()
    }
    
    func loadNextLevel(currentLevelFadedCompletion: (() -> Void)!) {
        guard let next = levels[.Next] else { return }
        
        if next.parent == nil {
            next.nodes.forEach { $0.alpha = 0.0 }
            addChild(next)
        }
        
        startGameTextNode.run(SKAction.fadeOut(withDuration: 0.2))
        
        levelTextNode.removeAllActions()
        
        addLevel(state: .Next)
        
        levels[.Previous] = currentLevel()
        levels[.Current] = next
        
        fadeOutBoard(levelNode: levels[.Previous]!) {
            self.levels[.Previous]?.resetTilesToStartingPositions()
            self.levels[.Previous]?.removeFromParent()
            self.showLevelText()
            currentLevelFadedCompletion?()
        }

        loadbgNode(from: .Previous, to: .Current)
    }
    
    func loadbgNode(from: LevelState, to: LevelState) {
        if
            let bgTextureFrom = sceneDelegate.level(state: from)?.bgTextureType,
            let bgTextureTo = sceneDelegate.level(state: to)?.bgTextureType {
            
            if bgTextureFrom == bgTextureTo { return }
            
            var bgNodeFrom: SKSpriteNode!, bgNodeTo: SKSpriteNode!
            
            switch bgTextureFrom {
            case .bgTypeA:
                bgNodeFrom = self.bgNode
            case .bgTypeB:
                bgNodeFrom = self.bgNode2
            case .bgTypeC:
                bgNodeFrom = self.bgNode3
            default:
                break
            }
            
            switch bgTextureTo {
            case .bgTypeA:
                bgNodeTo = self.bgNode
            case .bgTypeB:
                bgNodeTo = self.bgNode2
            case .bgTypeC:
                bgNodeTo = self.bgNode3
            default:
                break
            }
            
            if bgNodeFrom.parent == nil { return }
            
            bgNodeTo.alpha = 0.0
            bgNodeTo.zPosition = -1
            bgNodeTo.position = view!.center
            
            if bgNodeTo.parent == nil {
                addChild(bgNodeTo)
            }
            
            let fadeInAction = SKAction.fadeIn(withDuration: 0.4)
            let fadeOutAction = SKAction.fadeOut(withDuration: 0.4)
            
            bgNodeTo.removeAllActions()
            bgNodeFrom.removeAllActions()
            
            bgNodeTo.run(fadeInAction)
            
            bgNodeFrom.run(fadeOutAction) {
                bgNodeFrom.removeFromParent()
            }
        }
    }
    
    func loadPreviousLevel(currentLevelFadedCompletion: (() -> Void)!) {
        guard let previous = levels[.Previous] else { return }
        
        if previous.parent == nil {
            previous.nodes.forEach { $0.alpha = 0.0 }
            addChild(previous)
        }
        
        levelTextNode.removeAllActions()
        
        addLevel(state: .Previous)
        
        levels[.Next] = currentLevel()
        levels[.Current] = previous
        
        fadeOutBoard(levelNode: levels[.Next]!) {
            self.levels[.Next]?.resetTilesToStartingPositions()
            self.levels[.Next]?.removeFromParent()
            self.showLevelText()
            currentLevelFadedCompletion?()
        }
        
        loadbgNode(from: .Next, to: .Current)
    }
    
    //MARK: - Private
    
    private func fadeOutBoard(levelNode: LevelNode, completion: (() -> Void)!) {
        swipeNode?.removeFromParent()
        
        let fadeDuration = 0.2
        
        for child in levelNode.board.children {
            child.removeAllActions()
            child.run(SKAction.fadeOut(withDuration: fadeDuration))
        }
        
       completion?()
    }
    
    private func setupLevelText() {
        levelTextNode.alpha = 0.0
        levelTextNode.verticalAlignmentMode = .center
        levelTextNode.fontSize = 35.0
        
        addChild(levelTextNode)
    }
    
    private func setupGameBoard() {
        addLevel(state: .Current)
        addLevel(state: .Previous)
        addLevel(state: .Next)
        
        let bgTexture = SKTexture(linearGradientWithAngle: 90 * CGFloat.pi / 180, colors: [
            UIColor.hex(0x91358e),
            UIColor.hex(0x121b6c)
        ],
        locations: [0,1.0],
        size: CGSize(width: size.width, height: size.height))
        
        bgNode = SKSpriteNode(texture: bgTexture)
        bgNode.zPosition = -2
        bgNode.position = view!.center
        
        let bgTexture2 = SKTexture(linearGradientWithAngle: 90 * CGFloat.pi / 180, colors: [
            UIColor.hex(0xc34655),
            UIColor.hex(0x041142)
        ],
        locations: [0,1.0],
        size: CGSize(width: size.width, height: size.height))
                    
        bgNode2 = SKSpriteNode(texture: bgTexture2)
        bgNode2.zPosition = -2
        bgNode2.position = view!.center
                       
       let bgTexture3 = SKTexture(linearGradientWithAngle: 90 * CGFloat.pi / 180, colors: [
           UIColor.hex(0x347264),
           UIColor.hex(0x041b2d)
       ],
       locations: [0,1.0],
       size: CGSize(width: size.width, height: size.height))
       
        bgNode3 = SKSpriteNode(texture: bgTexture3)
        bgNode3.zPosition = -2
        bgNode3.position = view!.center
        
        switch sceneDelegate.level(state: .Current)?.bgTextureType {
        case .bgTypeA:
            addChild(bgNode)
        case .bgTypeB:
            addChild(bgNode2)
        case .bgTypeC:
            addChild(bgNode3)
        default:
            break
        }
       
        let blastNode = SKEmitterNode(fileNamed: "particleA.sks")!
        blastNode.numParticlesToEmit = 0
        blastNode.zPosition = -1
        blastNode.particlePositionRange = CGVector(
            dx: view!.bounds.size.width,
            dy: view!.bounds.size.height / 2
        )
        blastNode.particleAlphaSequence = SKKeyframeSequence(keyframeValues: [0.0, 1.0, 0.0], times: [0.0, 0.5, 1.0])
        blastNode.position = CGPoint(x: view!.center.x, y: view!.bounds.size.height * 0.25)
        
        let blastNode2 = SKEmitterNode(fileNamed: "particleB.sks")!
        blastNode2.numParticlesToEmit = 0
        blastNode2.zPosition = -1
        blastNode2.particlePositionRange = CGVector(
            dx: view!.bounds.size.width,
            dy: view!.bounds.size.height / 2
        )
        blastNode2.particleAlphaSequence = SKKeyframeSequence(keyframeValues: [0.0, 1.0, 0.0], times: [0.0, 0.5, 1.0])
        blastNode2.position = CGPoint(x: view!.center.x, y: view!.bounds.size.height * 0.75)
        
        addChild(blastNode)
        addChild(blastNode2)
        
        let blastNode3 = SKEmitterNode(fileNamed: "particleA.sks")!
        blastNode3.numParticlesToEmit = 0
        blastNode3.zPosition = -1
        blastNode3.particlePositionRange = CGVector(
            dx: view!.bounds.size.width,
            dy: view!.bounds.size.height / 2
        )
        blastNode3.particleAlphaSequence = SKKeyframeSequence(keyframeValues: [0.0, 1.0, 0.0], times: [0.0, 0.5, 1.0])
        blastNode3.position = CGPoint(x: view!.center.x, y: view!.bounds.size.height * 0.75)
        
        let blastNode4 = SKEmitterNode(fileNamed: "particleB.sks")!
        blastNode4.numParticlesToEmit = 0
        blastNode4.zPosition = -1
        blastNode4.particlePositionRange = CGVector(
            dx: view!.bounds.size.width,
            dy: view!.bounds.size.height / 2
        )
        blastNode4.particleAlphaSequence = SKKeyframeSequence(keyframeValues: [0.0, 1.0, 0.0], times: [0.0, 0.5, 1.0])
        blastNode4.position = CGPoint(x: view!.center.x, y: view!.bounds.size.height * 0.25)
        
        addChild(blastNode3)
        addChild(blastNode4)
    }
    
    private func showLevelText() {
        levelTextNode.text = "#\(currentLevel().level.id)"
        levelTextNode.fontColor = .white
        levelTextNode.position = CGPoint(x: view!.center.x, y: view!.center.y * 1.85)
        levelTextNode.alpha = 0.5
        levelTextNode.setScale(0.84)
        levelTextNode.zPosition = 100
        levelTextNode.removeAllActions()

        let fadeInAction = SKAction.fadeIn(withDuration: 0.3)
        let scaleAction = SKAction.scale(to: 1.0, duration: 1)
        levelTextNode.run(SKAction.group([fadeInAction,scaleAction]))
        
        let isLevelCompleted = sceneDelegate.isLevelCompleted(id: currentLevel().level.id)
        
        leftStar.removeAllActions()
        centerStar.removeAllActions()
        rightStar.removeAllActions()
        
        if isLevelCompleted {
            leftStar.run(SKAction.fadeIn(withDuration: 0.3))
            centerStar.run(SKAction.fadeIn(withDuration: 0.3))
            rightStar.run(SKAction.fadeIn(withDuration: 0.3))
        }
        else {
            leftStar.run(SKAction.fadeOut(withDuration: 0.3))
            centerStar.run(SKAction.fadeOut(withDuration: 0.3))
            rightStar.run(SKAction.fadeOut(withDuration: 0.3))
        }

        self.fadeInLevel()
    }
    
    private func fadeInLevel() {
        let levelNode = currentLevel()
        let tiles = levelNode.nodes
        
        let fadeInAction = SKAction.fadeIn(withDuration: 1)
        fadeInAction.timingMode = .easeInEaseOut
        
        for node in tiles { node.run(fadeInAction) }
        
        if levelNode.level.id == 1 {
            showSwipeGesture(direction: .Right)
        }
        
        self.sceneDelegate?.sceneLoaded()
    }
    
    private func addLevel(state: LevelState) {
        if let level = sceneDelegate.level(state: state) {
            addTiles(
                level: level,
                levelNode: addLevelNode(level: level, state: state)
            )
        }
    }
    
    private func addTiles(level: Level, levelNode: LevelNode) {
        for (index,tile) in level.board.enumerated() {
            let row = index / level.cols
            let col = index % level.cols
            let coordinate = BoardCoordinate(row: row, col: col)
            levelNode.addTile(tile: tile, coordinate: coordinate)
        }
    }
    
    private func currentLevel() -> LevelNode {
        return levels[.Current]!
    }
    
    private func addLevelNode(level: Level, state: LevelState) -> LevelNode {
        let levelNode = LevelNode(level: level)
                
        levels[state] = levelNode
        
        return levelNode
    }
    
    private func coordinateFromLocation(location: (Int,Int)) -> BoardCoordinate {
        let (row,col) = location
        return BoardCoordinate(row: row, col: col)
    }
    
    private func showSwipeGesture(direction: Direction) {
        switch direction {
        case .Right:
            swipeNode = SKSpriteNode(imageNamed: "swipe")
            swipeNode.position.x = view!.center.x - (1.5 * currentLevel().boardData.tileWidth)
            swipeNode.position.y = view!.bounds.size.height / 2.0 - currentLevel().level.boardData.tileWidth
            swipeNode.name = "swipeNode"
            addChild(swipeNode)
            
            let tileWidth = currentLevel().boardData.tileWidth

            let moveAction = SKAction.moveBy(x: 0, y: tileWidth * 2, duration: 1.0)
            let resetAction = SKAction.moveBy(x: 0, y: -tileWidth * 2, duration: 0.0)
            
            let waitFadeAction = SKAction.wait(forDuration: 0.5)
            let fadeOutAction = SKAction.fadeOut(withDuration: 0.5)
            let fadeInAction = SKAction.fadeIn(withDuration: 0.0)
            let waitResetAction = SKAction.wait(forDuration: 0.4)
            
            let moveSequence = SKAction.sequence([
                moveAction,
                waitResetAction,
                resetAction
            ])
            
            let fadeSequence = SKAction.sequence([
                waitFadeAction,
                fadeOutAction,
                waitResetAction,
                fadeInAction
            ])
            
            swipeNode.run(SKAction.repeatForever(SKAction.group([
                moveSequence,
                fadeSequence
            ])))
        default:
            break
        }
    }
}
