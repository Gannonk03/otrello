//
//  Menu.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

class Menu : UIView {
    let sound: MenuButton
    private let rate: MenuButton
    private let redo: MenuButton
    private let closeArrow: UIView
    private let closedSpacing: CGFloat = 9.0
    private let openSpacing: CGFloat = 20.0
    private let animateYDistance: CGFloat = 55.0
    private let paddingInset: CGFloat = 45.0
    private let startScale: CGFloat = 0.12
    private let closeArrowSpacing: CGFloat
    
    var setSoundSetting: (() -> Void)!
    
    weak var delegate: MenuDelegate?
    
    init(buttonSize: CGFloat) {
        let redoView = Redo()
        redoView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        let star = Rate()
        star.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        closeArrow = Arrow(fillColor: UIColor.init(white: 1, alpha: 0.5))
        closeArrow.transform = CGAffineTransform(scaleX: 0.375, y: 0.375)
            .concatenating(CGAffineTransform(rotationAngle: .pi / 2)
        )
        
        closeArrow.alpha = 0.0
        closeArrow.isUserInteractionEnabled = false
        
        sound = MenuButton(buttonSize: buttonSize, startScale: startScale,
            view: VolumeView()
        )
        
        rate = MenuButton(buttonSize: buttonSize, startScale: startScale,
            view: star
        )
        
        redo = MenuButton(buttonSize: buttonSize, startScale: startScale,
            view: redoView
        )
        
        closeArrowSpacing = buttonSize / 3.0
        
        super.init(frame: CGRect(
            x: 0,
            y: 0,
            width: 3 * buttonSize * startScale + paddingInset,
            height: buttonSize * startScale + paddingInset)
        )
        
        sound.center = center
        addSubview(sound)
        
        redo.center = center
        redo.frame.origin.x += closedSpacing
        addSubview(redo)
        
        rate.center = center
        rate.frame.origin.x -= closedSpacing
        addSubview(rate)
        
        closeArrow.center = center
        closeArrow.frame.origin.y -= closeArrowSpacing
        addSubview(closeArrow)
        
        let volumeTapped = UITapGestureRecognizer(
            target: self,
            action: #selector(Menu.volumeTapped))
        
        let rateTapped = UITapGestureRecognizer(
            target: self,
            action: #selector(Menu.rateTapped))
        
        let redoTapped = UITapGestureRecognizer(
            target: self,
            action: #selector(Menu.redoTapped))

        sound.addGestureRecognizer(volumeTapped)
        rate.addGestureRecognizer(rateTapped)
        redo.addGestureRecognizer(redoTapped)
        
        setSoundSetting = {[weak self] in self?.volumeOff() }
    }
    
    private func volumeOff() {
        delegate?.volumeOff()
        setSoundSetting = {[weak self] in self?.volumeOn() }
    }
    
    private func volumeOn() {
        delegate?.volumeOn()
        setSoundSetting = {[weak self] in self?.volumeOff() }
    }
    
    @objc func volumeTapped() {
        if !sound.isEnabled { return }
        
        isUserInteractionEnabled = false
        disableMenuButtons()
        
        sound.tapped {
            self.isUserInteractionEnabled = true
            self.enableMenuButtons()
        }
        
        setSoundSetting()
    }
    
    @objc func rateTapped() {
        if !rate.isEnabled { return }
        
        isUserInteractionEnabled = false
        disableMenuButtons()
        
        rate.tapped {
            self.isUserInteractionEnabled = true
            self.enableMenuButtons()
        }
        
        delegate?.rateSelected()
    }
    
    @objc func redoTapped() {
        if !redo.isEnabled { return }
        
        isUserInteractionEnabled = false
        disableMenuButtons()

        let onCompletion = delegate?.redoSelected()
        redo.tapped { onCompletion?() }
    }
    
     override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if sound.isActive() && sound.frame.contains(point) {
            return sound
        }
        
        if rate.isActive() && rate.frame.contains(point) {
            return rate
        }
        
        if redo.isActive() && redo.frame.contains(point) {
            return redo
        }
        
        return super.hitTest(point, with: event)
    }
    
    func setRedoEnabled(enabled: Bool) {
        redo.isEnabled = enabled
    }

    func open() {
        sound.open(
            xOffset: 0,
            yOffset: -animateYDistance,
            delay: 0,
            completion: nil
        )
        rate.open(
            xOffset: -rate.bounds.size.width * 1.6,
            yOffset: -animateYDistance,
            delay: 0.09,
            completion: nil
        )
        redo.open(
            xOffset: redo.bounds.size.width * 1.6,
            yOffset: -animateYDistance,
            delay: 0.09,
            completion: nil
        )
        
        animateCloseArrowOpen()
    }
    
    func close() {
        rate.close(
            xOffset: rate.bounds.size.width * 1.6,
            yOffset: animateYDistance,
            delay: 0,
            completion: nil
        )
        redo.close(
            xOffset: -redo.bounds.size.width * 1.6,
            yOffset: animateYDistance,
            delay: 0,
            completion: nil
        )
        sound.close(xOffset: 0, yOffset: animateYDistance, delay: 0.06) {
            self.isUserInteractionEnabled = true
            self.enableMenuButtons()
        }
        
        animateCloseArrowClosed()
    }
    
    private func animateCloseArrowOpen() {
        UIView.animate(
            withDuration: 0.83,
            delay: 0.15,
            usingSpringWithDamping: 0.43,
            initialSpringVelocity: 0.3,
            options: .curveEaseOut,
            animations: {
                self.closeArrow.alpha = 0.7
                self.closeArrow.frame.origin.y += self.closeArrowSpacing
            }, completion: nil
        )
    }
    
    private func animateCloseArrowClosed() {
        UIView.animate(withDuration: 0.15, animations: {
            self.closeArrow.alpha = 0.0
            self.closeArrow.frame.origin.y += 15.0
            self.closeArrow.transform =
                CGAffineTransform(scaleX: 0.2, y: 0.2).concatenating(
                CGAffineTransform(rotationAngle: .pi / 2)
            )
            }, completion: {completed in
                self.closeArrow.transform =
                    CGAffineTransform(scaleX: 0.375, y: 0.375).concatenating(
                    CGAffineTransform(rotationAngle: .pi / 2)
                )
                self.closeArrow.frame.origin.y -= self.closeArrowSpacing + 15.0
        })
    }
    
    private func disableMenuButtons() {
        sound.isEnabled = false
        rate.isEnabled = false
        redo.isEnabled = false
    }
    
    private func enableMenuButtons() {
        sound.isEnabled = true
        rate.isEnabled = true
        redo.isEnabled = true
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
