//
//  TileView.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

class TileView: UIView {
    private let dropShadowHeight: CGFloat = 3.5
    
    init(color: UIColor, length: CGFloat, radius: CGFloat) {
        super.init(frame: CGRect(x: 0, y: 0, width: length, height: length))
        
        layer.cornerRadius = radius
        backgroundColor = color.hueify()
        
        addTopLayer(color: color, radius: radius)
    }
    
    private func addTopLayer(color: UIColor, radius: CGFloat) {
        var frame = self.bounds
        frame.size.height -= dropShadowHeight
        
        let topLayer = UIView(frame: frame)
        topLayer.backgroundColor = color
        topLayer.layer.cornerRadius = radius
        
        addSubview(topLayer)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
