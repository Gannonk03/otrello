//
//  MenuButton.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

class MenuButton: UIView {
    private let startScale: CGFloat
    private let animateDuration: Double = 0.48
    let imageView: UIView
    
    var isEnabled: Bool = true
    
    init(
        buttonSize: CGFloat,
        startScale: CGFloat,
        view: UIView,
        forceOpen: Bool = false,
        xOffset: CGFloat = 0
    ) {
        self.startScale = startScale
        self.imageView = view
        
        super.init(frame: CGRect(
            x: 0,
            y: 0,
            width: buttonSize,
            height: buttonSize
        ))
        
        if forceOpen {
            imageView.center.y = center.y
            imageView.center.x = center.x + xOffset
            addSubview(imageView)
            isUserInteractionEnabled = true
            backgroundColor = UIColor.init(white: 1, alpha: 0.1)
            layer.borderColor = UIColor.init(white: 1, alpha: 0.5).cgColor
            layer.borderWidth = 1.5
            layer.cornerRadius = 0.5 * bounds.size.width
            return
        }
        
        imageView.alpha = 0.0
        imageView.center = self.center
        
        addSubview(imageView)
    
        backgroundColor = UIColor.init(white: 1, alpha: 0.5)
        layer.borderColor = UIColor.init(white: 1, alpha: 0.5).cgColor
        layer.borderWidth = 1.5
        layer.cornerRadius = 0.5 * bounds.size.width
        transform = CGAffineTransform(scaleX: startScale, y: startScale)
        
        isUserInteractionEnabled = false
    }

    func tapped(completion: (() -> Void)!) {
        if let toggleView = imageView as? ToggleView {
            toggleView.toggle()
        }
        
        UIView.animate(withDuration: 0.18, animations: {
            self.transform = CGAffineTransform(scaleX: 0.88, y: 0.88)
            }, completion: { done in
                if !done { return }
                UIView.animate(withDuration: 0.16, animations: {
                    self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: { finished in completion?()
            })
        })
    }
    
    func open(
        xOffset: CGFloat,
        yOffset: CGFloat,
        delay: Double,
        completion: (() -> Void)!
    ) {
        UIView.animate(
            withDuration: animateDuration,
            delay: delay,
            usingSpringWithDamping: 0.65,
            initialSpringVelocity: 0.5,
            options: .curveEaseOut,
            animations: {
                self.backgroundColor = UIColor.init(white: 1, alpha: 0.1)
                self.layer.borderColor = UIColor.init(white: 1, alpha: 0.5).cgColor
                self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.frame = self.frame.applying(CGAffineTransform(
                    translationX: xOffset,
                    y: yOffset
                ))
                self.imageView.alpha = 1.0
            }, completion: { done in completion?()
        })
    }
    
    func close(
        xOffset: CGFloat,
        yOffset: CGFloat,
        delay: Double,
        completion: (() -> Void)!
    ) {
        layer.removeAllAnimations()
        UIView.animate(
            withDuration: animateDuration,
            delay: delay,
            usingSpringWithDamping: 0.75,
            initialSpringVelocity: 0.5,
            options: .curveEaseOut,
            animations: {
                self.backgroundColor = UIColor.init(white: 1, alpha: 0.5)
                self.layer.borderColor = UIColor.init(white: 1, alpha: 0.5).cgColor
                self.transform = CGAffineTransform(
                    scaleX: self.startScale,
                    y: self.startScale
                )
                self.frame = self.frame.applying(CGAffineTransform(
                    translationX: xOffset,
                    y: yOffset
                ))
                self.imageView.alpha = 0.0
            }, completion: { done in completion?()
        })
    }
    
    func isActive() -> Bool {
        return transform.a == 1.0 && isEnabled
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
