
//
//  CheckMarkView.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit

protocol CheckMarkDelegate : class {
    func loadNextLevel(checkMark: CheckMark)
}

class CheckMark: UIView {
    private var check: UIImageView
    private var nextArrow: UIImageView
    
    weak var delegate: CheckMarkDelegate?
    
    init() {
        check = UIImageView(image: UIImage(named: "check"))
        nextArrow = UIImageView(image: UIImage(named: "nextArrow"))
        
        let ringImg = UIImage(named: "ring")?.withRenderingMode(.alwaysTemplate)
        let ring = UIImageView(image: ringImg)
        ring.tintColor = Color.moverColor(color: .Red)
        
        super.init(frame: .zero)
        
        clipsToBounds = true
        
        frame.size = ring.bounds.size
        backgroundColor = UIColor.clear
        
        check.center = center
        
        nextArrow.center.y = center.y
        nextArrow.alpha = 0.0
        nextArrow.frame.origin.x = bounds.size.width
        
        addSubview(ring)
        addSubview(nextArrow)
        addSubview(check)
    }
    
    func revealNextLevelOption(delay: Double = 0.0) {
        UIView.animate(
            withDuration: 0.4,
            delay: delay,
            options: .curveEaseOut,
            animations: { self.check.alpha = 0.0 },
            completion: nil
        )
        
        UIView.animate(
            withDuration: 0.4,
            delay: delay + 0.1,
            options: [],
            animations: {
                self.nextArrow.frame.origin.x =
                self.bounds.size.width / 2.0 -
                (self.nextArrow.bounds.size.width / 2.0)
            },
            completion: nil
        )
        
        UIView.animate(
            withDuration: 0.4,
            delay: delay + 0.1,
            options: [],
            animations: {
                let width = self.check.bounds.size.width
                self.check.frame.origin.x = -width
            },
            completion: nil
        )
        
        UIView.animate(
            withDuration: 0.3,
            delay: delay + 0.3,
            options: .curveEaseOut,
            animations: { self.nextArrow.alpha = 1.0 },
            completion: { done in self.addTapGesture() }
        )
    }
    
    @objc func tapped(sender: UITapGestureRecognizer) {
        removeGestureRecognizer(sender)
        UIView.animate(withDuration: 0.18, animations: {
            self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            }, completion: { done in
                UIView.animate(withDuration: 0.16, animations: {
                    self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    }, completion: { finished in
                        self.delegate?.loadNextLevel(checkMark: self)
                })
        })
    }
    
    private func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(tapped)
        )
        
        addGestureRecognizer(tapGesture)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
