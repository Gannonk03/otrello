//
//  GameView.swift
//  X
//
//  Created by Kevin Gannon on 12/27/19.
//  Copyright © 2019 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

protocol GameViewDelegate : MenuDelegate, LevelNaviationDelegate {
    func slide(direction: Direction)
    func menuWillOpen() -> Bool
    func menuWillClose()
    func boardWidth() -> CGFloat
}

enum LevelNavState {
    case WaitForMinTimeNotApproved
    case WaitForMinTimeApproved(disabled: Bool)
    case WaitingForApproval
    case NotWaiting
}

class GameView: UIView, UIGestureRecognizerDelegate, CheckMarkDelegate  {
    private var menu: Menu
    private var menuHeader: UILabel
    private var leftNavigationArrow: LevelNavigationButton
    private var rightNavigationArrow: LevelNavigationButton
    private var handleMenuTap: () -> Void = {}
    private var handleViewTap: () -> Void = {}
    private var swipeRecognizers = [UISwipeGestureRecognizer]()
    private var leftNavState = LevelNavState.NotWaiting
    private var rightNavState = LevelNavState.NotWaiting
    private var boardView: SKView
    private var blurView: UIVisualEffectView
    private var menuVolume: MenuButton
    private var menuRate: MenuButton
    
    weak var delegate: GameViewDelegate? { didSet { menu.delegate = delegate }}
    
    override init(frame: CGRect) {
        menu = Menu(buttonSize: 45.0)
        
        leftNavigationArrow = LevelNavigationButton(direction: .Left)
        rightNavigationArrow = LevelNavigationButton(direction: .Right)
        
        leftNavigationArrow.alpha = 0.0
        rightNavigationArrow.alpha = 0.0
        
        boardView = SKView(frame: frame)
        boardView.ignoresSiblingOrder = true
        
        blurView = UIVisualEffectView()
        
        menuHeader = UILabel(frame: .zero)
        
        let star = Rate()
        star.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                
        menuVolume = MenuButton(buttonSize: 45.0, startScale: 1.0, view: VolumeView(), forceOpen: true)
        menuRate = MenuButton(buttonSize: 45.0, startScale: 1.0, view: star, forceOpen: true)
        
        super.init(frame: frame)
        
        handleViewTap = { [weak self] in self?.startGame() }
        
        menu.center.x = center.x
        menu.frame.origin.y = bounds.size.height - 45.0
        menu.alpha = 0.0
        
        menuVolume.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(homeVolumeTapped)))
        menuVolume.center.x = center.x * 0.8
        menuVolume.center.y = center.y * 1.8
        
        menuRate.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(homeRateTapped)))
        menuRate.center.x = center.x * 1.2
        menuRate.center.y = center.y * 1.8

        menuHeader.text = "Otrello"
        menuHeader.textColor = .white
        menuHeader.font = UIFont(name: "NasalizationRg-Regular", size: 60)!
        menuHeader.sizeToFit()
        menuHeader.center.x = center.x
        menuHeader.center.y = center.y * 0.6
        
        blurView.frame = frame
        blurView.isUserInteractionEnabled = false
        
        leftNavigationArrow.center.y = center.y * 0.15
        rightNavigationArrow.center.y = center.y * 0.15
        leftNavigationArrow.center.x = center.x * 0.45
        rightNavigationArrow.center.x = bounds.width - leftNavigationArrow.center.x
        
        leftNavigationArrow.addTarget(
            self,
            action: #selector(GameView.previousLevelSelected),
            for: .touchUpInside
        )
        
        rightNavigationArrow.addTarget(
            self,
            action: #selector(GameView.nextLevelSelected),
            for: .touchUpInside
        )
        
        let menuTapped = UITapGestureRecognizer(
            target: self,
            action: #selector(GameView.menuTapped)
        )
        
        menu.addGestureRecognizer(menuTapped)
        
        let viewTapped = UITapGestureRecognizer(
            target: self,
            action: #selector(GameView.viewTapped)
        )
        
        viewTapped.delegate = self
        addGestureRecognizer(viewTapped)
        
        handleMenuTap = { [weak self] in self?.openMenu() }
        
        addSubview(boardView)
        addSubview(leftNavigationArrow)
        addSubview(rightNavigationArrow)
        addSubview(blurView)
        addSubview(menu)
        addSubview(menuHeader)
        addSubview(menuVolume)
        addSubview(menuRate)
       
        addSwipeControls()
    }
    
    @objc func homeVolumeTapped() {
        let toggle = menu.sound.imageView as? ToggleView
        
        toggle?.toggle()
        
        menu.setSoundSetting()
        
        menuVolume.tapped(completion: nil)
    }
    
    @objc func homeRateTapped() {
        menu.delegate?.rateSelected()
        menuRate.tapped(completion: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func presentScene(scene: SKScene) {
        boardView.presentScene(scene)
    }
    
    func loadNextLevel(checkMark: CheckMark) {
        nextLevelSelected()
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0.2,
            options: [],
            animations: { self.blurView.effect = nil },
            completion: { done in checkMark.removeFromSuperview() }
        )
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0.0,
            options: [],
            animations: { checkMark.alpha = 0.0 },
            completion: { done in checkMark.removeFromSuperview() }
        )
    }
    
    func showLevelCompleted(delay: Double = 0.45) {
        UIView.animate(
            withDuration: 0.4,
            delay: delay,
            options: [],
            animations: {
                self.leftNavigationArrow.isUserInteractionEnabled = false
                self.rightNavigationArrow.isUserInteractionEnabled = false
                self.blurView.effect = UIBlurEffect(style: .dark)
        },
            completion: nil
        )
        
        let checkMark = CheckMark()
        checkMark.delegate = self
        checkMark.alpha = 0
        checkMark.center = center
        checkMark.center.y += checkMark.bounds.size.height / 2.0

        addSubview(checkMark)
        
        UIView.animate(
            withDuration: 0.4,
            delay: delay + 0.3,
            options: [],
            animations: { checkMark.center.y = self.center.y },
            completion: nil
        )
        
        UIView.animate(
            withDuration: 0.5,
            delay: delay + 0.35,
            options: .curveEaseInOut,
            animations: { checkMark.alpha = 1 },
            completion: { done in checkMark.revealNextLevelOption() }
        )
    }
    
    func gestureRecognizer(
        _ gestureRecognizer: UIGestureRecognizer,
        shouldReceive touch: UITouch
    ) -> Bool {
        
        let location = touch.location(in: self)
        
        if leftNavigationArrow.frame.contains(location) {
            return false
        }
        
        if rightNavigationArrow.frame.contains(location) {
            return false
        }
        
        return true
    }
    
    func updatePreviousLevelOption(disabled: Bool) {
        if disabled {
            leftNavigationArrow.setDisabled(disabled)
            return
        }
        
        switch leftNavState {
        case .WaitingForApproval:
            leftNavigationArrow.setDisabled(disabled)
        case .WaitForMinTimeNotApproved:
            leftNavState = .WaitForMinTimeApproved(disabled: disabled)
        default:
            return
        }
    }
    
    func updateNextLevelOption(disabled: Bool) {
        if disabled {
            rightNavigationArrow.setDisabled(disabled)
            return
        }
        
        switch rightNavState {
        case .WaitForMinTimeNotApproved:
            rightNavState = .WaitForMinTimeApproved(disabled: disabled)
        default:
            rightNavigationArrow.setDisabled(disabled)
        }
    }
    
    func startGame() {
        handleViewTap = {}
        nextLevelSelected()
        UIView.animate(withDuration: 0.2) {[weak self] in
            self?.menu.alpha = 1.0
            self?.menuHeader.alpha = 0.0
            self?.menuRate.alpha = 0.0
            self?.menuVolume.alpha = 0.0
        }
    }
    
    func setRedoEnabled(enabled: Bool) {
        menu.setRedoEnabled(enabled: enabled)
    }
    
    @objc func viewTapped() {
        handleViewTap()
    }
    
    @objc func menuTapped() {
        handleMenuTap()
    }
    
    @objc func upSwipe() {
        delegate?.slide(direction: .Up)
    }
    
    @objc func downSwipe() {
        delegate?.slide(direction: .Down)
    }
    
    @objc func leftSwipe() {
        delegate?.slide(direction: .Left)
    }
    
    @objc func rightSwipe() {
        delegate?.slide(direction: .Right)
    }
    
    func openMenu() {
        if delegate?.menuWillOpen() ?? true {
            menu.open()
            removeSwipeRecognizers()
            handleMenuTap = { [weak self] in self?.closeMenu() }
            handleViewTap = { [weak self] in self?.closeMenu() }
        }
    }
    
    func closeMenu() {
        delegate?.menuWillClose()
        addSwipeControls()
        menu.close()
        handleMenuTap = { [weak self] in self?.openMenu() }
        handleViewTap = {}
    }
    
    @objc func previousLevelSelected() {
        delayTouchOnNavigation()
        delegate?.previousLevelSelected()
    }
    
    @objc func nextLevelSelected() {
        delayTouchOnNavigation()
        delegate?.nextLevelSelected()
    }
    
    //MARK: - Private 
    private func addSwipeControls() {
        let upSwipe = UISwipeGestureRecognizer(
            target: self,
            action: #selector(GameView.upSwipe)
        )
        upSwipe.direction = .up
        addGestureRecognizer(upSwipe)
        
        let downSwipe = UISwipeGestureRecognizer(
            target: self,
            action: #selector(GameView.downSwipe)
        )
        downSwipe.direction = .down
        addGestureRecognizer(downSwipe)
        
        let leftSwipe = UISwipeGestureRecognizer(
            target: self,
            action: #selector(GameView.leftSwipe)
        )
        leftSwipe.direction = .left
        addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(
            target: self,
            action: #selector(GameView.rightSwipe)
        )
        rightSwipe.direction = .right
        addGestureRecognizer(rightSwipe)
        
        swipeRecognizers = [upSwipe,downSwipe,leftSwipe,rightSwipe]
    }
    
    private func removeSwipeRecognizers() {
        for swipe in swipeRecognizers {
            removeGestureRecognizer(swipe)
        }
    }
    
    private func delayTouchOnNavigation(animated: Bool = false) {
        leftNavigationArrow.isUserInteractionEnabled = false
        rightNavigationArrow.isUserInteractionEnabled = false
        
        leftNavState = .WaitForMinTimeNotApproved
        rightNavState = .WaitForMinTimeNotApproved
 
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            switch self.leftNavState {
            case let .WaitForMinTimeApproved(disabled: d) :
                self.leftNavigationArrow.setDisabled(d, animated: animated)
            default:
                self.leftNavState = .WaitingForApproval
            }
            
            switch self.rightNavState {
            case let .WaitForMinTimeApproved(disabled: d) :
                self.rightNavigationArrow.setDisabled(d, animated: animated)
            default:
                self.rightNavState = .WaitingForApproval
            }
        }
    }
}
